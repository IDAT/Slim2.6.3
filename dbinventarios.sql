-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-08-2018 a las 14:12:27
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbinventarios`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `prcAlmacenSelect` ()  BEGIN
	SELECT * FROM tbinvmaealmacen;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcArticuloSelect` ()  BEGIN
	SELECT a.*, u.und_descri,
    ( 0
    -- (SELECT IFNULL(SUM(gid_cantid),0) FROM tbinvmovguiadetalle WHERE gid_artcod = a.art_codigo AND gid_tipo='E' AND gid_estcod <> 0) - 
    -- (SELECT IFNULL(SUM(gid_cantid),0) FROM tbinvmovguiadetalle WHERE gid_artcod = a.art_codigo AND gid_tipo='S' AND gid_estcod <> 0) 
    ) AS art_stoact
	FROM tbinvmaarticulo a 
    LEFT JOIN tbinvmaeunidades u ON art_undcod = und_codigo;	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcArticuloSelectByDescri` (IN `in_des` VARCHAR(10))  BEGIN
	SELECT * 
    FROM tbinvmaarticulo
    WHERE art_descri like CONCAT('%',in_des,'%');
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcGuiaCabeceraInsert` (IN `in_tipo` CHAR(1), IN `in_tracod` CHAR(3), `in_observ` VARCHAR(200), `in_usucod` CHAR(7), `in_estcod` INT, OUT `out_correl` CHAR(14))  BEGIN
	-- DECLARE CORREL CHAR(14);
	SET out_correl = (SELECT 
				CONCAT('GUI-',YEAR(NOW()),SUBSTRING(CONCAT('00000',SUBSTRING(IFNULL(MAX(gic_correl), 0),-6) + 1),-6))
				FROM tbinvmovguiacabecera 
				WHERE SUBSTRING(gic_correl,5,4) = YEAR(NOW()));
   
    INSERT INTO tbinvmovguiacabecera
	(gic_correl, gic_fecreg, gic_tipo, gic_tracod, gic_observ, gic_usucod, gic_estcod)
	VALUES(out_correl, NOW(), in_tipo, in_tracod, in_observ, in_usucod, in_estcod);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcGuiaCabeceraSelect` ()  BEGIN
	SELECT c.*, t.tra_descri, u.usu_nombre, p.par_funcion  
    FROM tbinvmovguiacabecera c 
    INNER JOIN tbinvmaetransacciones t ON c.gic_tracod = t.tra_codigo 
    INNER JOIN tbusuarios u ON c.gic_usucod = u.usu_codigo
    INNER JOIN tbinvparametros p ON TRIM(c.gic_estcod) = TRIM(p.par_argume) AND p.par_codigo='T99' AND par_tipcod ='D';
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcGuiaCabeceraSelectByCorrel` (IN `in_correl` VARCHAR(14))  BEGIN
	SELECT c.*, t.tra_descri, u.usu_nombre, p.par_funcion  
    FROM tbinvmovguiacabecera c 
    INNER JOIN tbinvmaetransacciones t ON c.gic_tracod = t.tra_codigo 
    INNER JOIN tbusuarios u ON c.gic_usucod = u.usu_codigo
    INNER JOIN tbinvparametros p ON TRIM(c.gic_estcod) = TRIM(p.par_argume) AND p.par_codigo='T99' AND par_tipcod ='D'
    WHERE c.gic_correl = in_correl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcGuiaDetalleInsert` (IN `in_correl` CHAR(14), IN `in_tipo` CHAR(1), IN `in_tracod` CHAR(3), IN `in_artcod` CHAR(8), IN `in_cantid` DOUBLE, IN `in_estcod` INT(11))  BEGIN	 
    INSERT INTO tbinvmovguiadetalle  
    (gid_correl,gid_fecreg,gid_tipo,gid_tracod,gid_artcod,gid_cantid,gid_estcod) 
    VALUES(in_correl,NOW(),in_tipo,in_tracod,in_artcod,in_cantid,in_estcod);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcGuiaDetalleSelectByCorrel` (IN `in_correl` VARCHAR(14))  BEGIN
	SELECT d.*, a.art_descri, u.und_abrevi
    FROM tbinvmovguiadetalle d 
    INNER JOIN tbinvmaarticulo a ON d.gid_artcod = a.art_codigo 
    INNER JOIN tbinvmaeunidades u ON a.art_undcod = u.und_codigo
    WHERE gid_correl = in_correl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcTransacionSelectByTipo` (IN `in_tip` CHAR(1))  BEGIN
	SELECT * 
	FROM tbinvmaetransacciones 
    WHERE tra_tipo = in_tip;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioDelete` (IN `in_codigo` VARCHAR(7))  BEGIN
	DELETE FROM tbusuarios WHERE usu_codigo = in_codigo;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioInsert` (IN `in_nombre` VARCHAR(45), IN `in_passwd` VARCHAR(45), IN `in_descri` VARCHAR(45), IN `in_email` VARCHAR(45), IN `in_estcod` INT)  BEGIN	
	DECLARE CODIGO CHAR(7);

	SET CODIGO = (SELECT CONCAT('USU-', SUBSTRING(CONCAT ('00', SUBSTRING(MAX(usu_codigo),-3) + 1),-3)) FROM tbusuarios);
   
    INSERT into tbusuarios (usu_codigo, usu_nombre, usu_passwd, usu_descri, usu_email,usu_fecreg, usu_estcod) VALUES (CODIGO,in_nombre,in_passwd,in_descri,in_email,NOW(),in_estcod);

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioLogin` (IN `in_nombre` VARCHAR(45), IN `in_passwd` VARCHAR(45), OUT `out_codigo` CHAR(7), OUT `out_descri` VARCHAR(100), OUT `out_email` VARCHAR(50), OUT `out_estcod` INTEGER)  BEGIN
   SELECT usu_codigo, usu_descri, usu_email, usu_estcod
   INTO out_codigo, out_descri, out_email,out_estcod
   FROM tbusuarios where usu_nombre = in_nombre and usu_passwd = in_passwd;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioSelect` ()  BEGIN
	SELECT * FROM tbusuarios;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioSelectByCodigo` (IN `in_codigo` VARCHAR(7))  BEGIN
	SELECT * FROM tbusuarios WHERE usu_codigo = in_codigo;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `prcUsuarioUpdate` (IN `in_nombre` VARCHAR(45), IN `in_passwd` VARCHAR(45), IN `in_descri` VARCHAR(45), IN `in_email` VARCHAR(45), IN `in_estcod` INT, IN `in_codigo` VARCHAR(7))  BEGIN
	UPDATE tbusuarios 
	SET usu_nombre = in_nombre, usu_nombre = in_nombre, usu_passwd = in_passwd, usu_descri = in_descri, usu_email = in_email, usu_estcod = in_estcod 
	WHERE usu_codigo = in_codigo;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaarticulo`
--

CREATE TABLE `tbinvmaarticulo` (
  `art_codigo` char(8) NOT NULL,
  `art_lincod` char(4) NOT NULL,
  `art_catcod` char(3) NOT NULL,
  `art_descri` varchar(200) DEFAULT NULL,
  `art_undcod` char(3) NOT NULL,
  `art_stomin` double DEFAULT NULL,
  `art_stomax` double DEFAULT NULL,
  `art_usucod` char(7) NOT NULL,
  `art_fecreg` datetime DEFAULT NULL,
  `art_estcod` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmaarticulo`
--

INSERT INTO `tbinvmaarticulo` (`art_codigo`, `art_lincod`, `art_catcod`, `art_descri`, `art_undcod`, `art_stomin`, `art_stomax`, `art_usucod`, `art_fecreg`, `art_estcod`) VALUES
('01010001', '0101', '001', 'DELL OPTIPLEX 9020', '001', 0, 0, 'USU-001', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaealmacen`
--

CREATE TABLE `tbinvmaealmacen` (
  `alm_codigo` char(7) NOT NULL,
  `alm_descri` varchar(45) DEFAULT NULL,
  `alm_latitu` double DEFAULT NULL,
  `alm_longit` double DEFAULT NULL,
  `alm_codest` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbinvmaealmacen`
--

INSERT INTO `tbinvmaealmacen` (`alm_codigo`, `alm_descri`, `alm_latitu`, `alm_longit`, `alm_codest`) VALUES
('ALM-001', 'PIURA', -5.151531849252648, -80.67140261548195, 1),
('ALM-002', 'IQUITOS', -3.705705196764481, -73.26661745923195, 1),
('ALM-003', 'CHICLAYO', -6.768664265112981, -79.85841433423195, 1),
('ALM-004', 'CAJAMARCA', -7.139450863546157, -78.54005495923195, 1),
('ALM-005', 'TARAPOTO', -6.463095911875107, -76.37453118086671, 1),
('ALM-006', 'TRUJILLO', -8.075947714971415, -79.03322258711671, 1),
('ALM-007', 'CHIMBOTE', -9.029789805277469, -78.6058639011095, 1),
('ALM-008', 'PUCALLPA', -8.356466580849139, -74.6068404636095, 1),
('ALM-009', 'HUARAZ', -9.485204403173753, -77.5511764011095, 1),
('ALM-010', 'HUANUCO', -9.91837147032543, -76.2547896823595, 1),
('ALM-011', 'LIMA', -12.032177428530888, -77.0458053073595, 1),
('ALM-012', 'AYACUCHO', -13.147263916923725, -74.2333053073595, 1),
('ALM-013', 'CUSCO', -13.489370675648072, -71.9920943698595, 1),
('ALM-014', 'AREQUIPA', -16.376383877103343, -71.5526412448595, 1),
('ALM-015', 'TACNA', -17.997287698009732, -70.24702776073116, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaecaracteristicas`
--

CREATE TABLE `tbinvmaecaracteristicas` (
  `car_codigo` char(3) NOT NULL,
  `car_descri` varchar(45) DEFAULT NULL,
  `car_usucod` char(7) NOT NULL,
  `car_fecreg` datetime DEFAULT NULL,
  `car_estcod` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmaecaracteristicas`
--

INSERT INTO `tbinvmaecaracteristicas` (`car_codigo`, `car_descri`, `car_usucod`, `car_fecreg`, `car_estcod`) VALUES
('001', 'COLOR', 'USU-001', '2018-01-01 00:00:00', 1),
('002', 'MARCA', 'USU-001', '2018-01-01 00:00:00', 1),
('003', 'MODELO', 'USU-001', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaecatalogo`
--

CREATE TABLE `tbinvmaecatalogo` (
  `cat_codigo` char(3) NOT NULL,
  `cat_descri` varchar(45) DEFAULT NULL,
  `cat_usucod` char(7) NOT NULL,
  `cat_fecreg` datetime DEFAULT NULL,
  `cat_estcod` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmaecatalogo`
--

INSERT INTO `tbinvmaecatalogo` (`cat_codigo`, `cat_descri`, `cat_usucod`, `cat_fecreg`, `cat_estcod`) VALUES
('001', 'CATALOGO CPU', 'USU-001', '2018-01-01 00:00:00', 1),
('002', 'CATALOGO MONITOR', 'USU-001', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaecataxcara`
--

CREATE TABLE `tbinvmaecataxcara` (
  `cxc_catcod` char(3) NOT NULL,
  `cxc_carcod` char(3) NOT NULL,
  `cxc_nrosec` char(3) NOT NULL,
  `cxc_obliga` int(11) DEFAULT NULL,
  `cxc_usucod` char(7) NOT NULL,
  `cxc_fecreg` datetime DEFAULT NULL,
  `cxc_estcod` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmaecataxcara`
--

INSERT INTO `tbinvmaecataxcara` (`cxc_catcod`, `cxc_carcod`, `cxc_nrosec`, `cxc_obliga`, `cxc_usucod`, `cxc_fecreg`, `cxc_estcod`) VALUES
('001', '001', '001', 1, 'USU-001', '2018-01-01 00:00:00', 1),
('001', '002', '002', 1, 'USU-001', '2018-01-01 00:00:00', 1),
('002', '001', '001', 1, 'USU-001', '2018-01-01 00:00:00', 1),
('002', '002', '002', 1, 'USU-001', '2018-01-01 00:00:00', 1),
('002', '003', '003', 1, 'USU-001', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaelinea`
--

CREATE TABLE `tbinvmaelinea` (
  `lin_codigo` char(4) NOT NULL COMMENT 'Código',
  `lin_descri` varchar(200) DEFAULT NULL COMMENT 'Descripción',
  `lin_usucod` char(7) NOT NULL,
  `lin_fecreg` datetime DEFAULT NULL,
  `lin_estcod` int(1) DEFAULT '1' COMMENT 'Estado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmaelinea`
--

INSERT INTO `tbinvmaelinea` (`lin_codigo`, `lin_descri`, `lin_usucod`, `lin_fecreg`, `lin_estcod`) VALUES
('0100', 'EQUIPOS DE COMPUTO', 'USU-001', '2018-01-01 00:00:00', 1),
('0101', 'CPU', 'USU-001', '2018-01-01 00:00:00', 1),
('0102', 'MONITORES', 'USU-001', '2018-01-01 00:00:00', 1),
('0103', 'TECLADO', 'USU-001', '2018-01-01 00:00:00', 1),
('0104', 'MOUSE', 'USU-001', '2018-01-01 00:00:00', 1),
('0105', 'IMPRESORAS', 'USU-001', '2018-01-01 00:00:00', 1),
('0200', 'MUEBLES DE OFICINA', 'USU-001', '2018-01-01 00:00:00', 1),
('0201', 'ESCRITORIO', 'USU-001', '2018-01-01 00:00:00', 1),
('0202', 'SILLA', 'USU-001', '2018-01-01 00:00:00', 1),
('0203', 'CAJONERA', 'USU-001', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaetransacciones`
--

CREATE TABLE `tbinvmaetransacciones` (
  `tra_codigo` char(3) NOT NULL,
  `tra_tipo` char(1) DEFAULT NULL,
  `tra_descri` varchar(45) DEFAULT NULL,
  `tra_fecreg` datetime DEFAULT NULL,
  `tra_estcod` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbinvmaetransacciones`
--

INSERT INTO `tbinvmaetransacciones` (`tra_codigo`, `tra_tipo`, `tra_descri`, `tra_fecreg`, `tra_estcod`) VALUES
('EXC', 'E', 'ENTRADA X COMPRA', '2018-01-01 00:00:00', 1),
('SXC', 'S', 'SALIDA X COMPRA', '2018-01-01 00:00:00', 1),
('SXD', 'S', 'SALIDA X DONACION', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmaeunidades`
--

CREATE TABLE `tbinvmaeunidades` (
  `und_codigo` char(3) NOT NULL,
  `und_descri` varchar(45) DEFAULT NULL,
  `und_abrevi` varchar(45) DEFAULT NULL,
  `und_usucod` char(7) NOT NULL,
  `und_fecreg` datetime DEFAULT NULL,
  `und_codest` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmaeunidades`
--

INSERT INTO `tbinvmaeunidades` (`und_codigo`, `und_descri`, `und_abrevi`, `und_usucod`, `und_fecreg`, `und_codest`) VALUES
('001', 'UNIDAD', 'UND', 'USU-001', '2018-01-01 00:00:00', 1),
('002', 'TONELADA', 'TN', 'USU-001', '2018-01-01 00:00:00', 1),
('003', 'KILOGRAMOS', 'KGS', 'USU-001', '2018-01-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmovguiacabecera`
--

CREATE TABLE `tbinvmovguiacabecera` (
  `gic_correl` char(20) NOT NULL,
  `gic_fecreg` datetime NOT NULL,
  `gic_tipo` varchar(45) NOT NULL,
  `gic_tracod` char(3) NOT NULL,
  `gic_observ` varchar(200) DEFAULT NULL,
  `gic_usucod` char(7) NOT NULL,
  `gic_estcod` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmovguiacabecera`
--

INSERT INTO `tbinvmovguiacabecera` (`gic_correl`, `gic_fecreg`, `gic_tipo`, `gic_tracod`, `gic_observ`, `gic_usucod`, `gic_estcod`) VALUES
('GUI-2018000001', '2018-01-01 00:00:00', 'E', 'EXC', 'IMPORTACION', 'USU-001', 1),
('GUI-2018000002', '2018-01-02 00:00:00', 'S', 'SXV', NULL, 'USU-001', 0),
('GUI-2018000003', '2018-01-02 00:00:00', 'S', 'SXD', NULL, 'USU-001', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmovguiadetalle`
--

CREATE TABLE `tbinvmovguiadetalle` (
  `gid_correl` char(20) NOT NULL,
  `gid_fecreg` datetime NOT NULL,
  `gid_tipo` varchar(45) NOT NULL,
  `gid_tracod` char(3) NOT NULL,
  `gid_artcod` char(8) NOT NULL,
  `gid_cantid` double DEFAULT NULL,
  `gid_estcod` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmovguiadetalle`
--

INSERT INTO `tbinvmovguiadetalle` (`gid_correl`, `gid_fecreg`, `gid_tipo`, `gid_tracod`, `gid_artcod`, `gid_cantid`, `gid_estcod`) VALUES
('GUI-2018000001', '2018-01-01 00:00:00', 'E', 'EXC', '01010001', 10, 1),
('GUI-2018000002', '2018-01-01 00:00:00', 'S', 'SXV', '01010001', 3, 0),
('GUI-2018000003', '2018-01-01 00:00:00', 'S', 'SXD', '01010001', 7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvmvespecificaciones`
--

CREATE TABLE `tbinvmvespecificaciones` (
  `vcc_artcod` char(8) NOT NULL,
  `vcc_catcod` char(3) NOT NULL,
  `vcc_carcod` char(3) NOT NULL,
  `vcc_nrosec` char(3) NOT NULL,
  `vcc_valor` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbinvmvespecificaciones`
--

INSERT INTO `tbinvmvespecificaciones` (`vcc_artcod`, `vcc_catcod`, `vcc_carcod`, `vcc_nrosec`, `vcc_valor`) VALUES
('01010001', '001', '001', '001', 'NEGRO'),
('01010001', '001', '002', '002', 'DELL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbinvparametros`
--

CREATE TABLE `tbinvparametros` (
  `par_codigo` char(3) NOT NULL COMMENT 'CODIGO DE PARAMETRO',
  `par_tipcod` char(1) NOT NULL COMMENT 'TIPO DE PARAMETRO (C) CABECERA (D) DETALLE',
  `par_argume` varchar(25) NOT NULL COMMENT 'ARGUMENTO',
  `par_funcion` varchar(250) DEFAULT NULL COMMENT 'FUNCION O DESCRIPCION DEL PARAMETRO',
  `par_usucod` char(10) DEFAULT NULL COMMENT 'USUARIO REGISTRO',
  `par_fecreg` datetime DEFAULT NULL COMMENT 'FECHA REGISTRO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLA DE PARAMETROS DEL SISTEMA';

--
-- Volcado de datos para la tabla `tbinvparametros`
--

INSERT INTO `tbinvparametros` (`par_codigo`, `par_tipcod`, `par_argume`, `par_funcion`, `par_usucod`, `par_fecreg`) VALUES
('T01', 'C', '-', 'TABLA ESTADOS', 'USU-001', '2018-01-01 00:00:00'),
('T01', 'D', '0', 'INACTIVO', 'USU-001', '2018-01-01 00:00:00'),
('T01', 'D', '1', 'ACTIVO', 'USU-001', '2018-01-01 00:00:00'),
('T99', 'C', '-', 'TABLA ESTADOS GUIAS', 'USU-001', '2018-01-01 00:00:00'),
('T99', 'D', '0', 'ANULADO', 'USU-001', '2018-01-01 00:00:00'),
('T99', 'D', '1', 'ACTIVO', 'USU-001', '2018-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbusuarios`
--

CREATE TABLE `tbusuarios` (
  `usu_codigo` char(7) NOT NULL,
  `usu_nombre` varchar(45) DEFAULT NULL,
  `usu_passwd` varchar(45) DEFAULT NULL,
  `usu_descri` varchar(100) DEFAULT NULL,
  `usu_email` varchar(50) DEFAULT NULL,
  `usu_fecreg` datetime DEFAULT NULL,
  `usu_imagen` text,
  `usu_estcod` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbusuarios`
--

INSERT INTO `tbusuarios` (`usu_codigo`, `usu_nombre`, `usu_passwd`, `usu_descri`, `usu_email`, `usu_fecreg`, `usu_imagen`, `usu_estcod`) VALUES
('USU-001', 'ATORRESCH', 'p@$$W0RD', 'ALEX TORRES', 'superahacker@gmail.com', '2018-01-01 00:00:00', 'iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAgAElEQVR4XtS9i5N0+3UdtE93n9PP6Znvee+1CixLFnZShGdKUiwbJ1Yck3KIgmIFmwplCgyJSSC4ILYSsIsEUFzBJFSqUkUqQKqCScg/RYWASrqP7zEz/TzdfQ611tr79/t1f/NdXckOsT9Va+bOo6f7nN/aj7XX3rv6xi98vp9PJzYf12ZdZ8e2tdvbe1uvN7bdHexw6u3UV3x0fWXW4dGb9Z1VdrKBnayyzgaV2aiqrPKHmVnfV/gx66znr/X8Wm+d4Skq6/E0VaWvVwMzPkwfrbJ61tj0amJXy6U1k7FVw6F1+P2+s+FwhD9hp9OJH4fDYfrblQ2s4hPhHz7D81n+fhV/qrcKn/Pv43Xyxw1vpqoG+h5ed9/rtXZ6D9UAf2tgg2pop/5gXX9Mzz8YDA0P/E3+To/Lit/nG+MX+LWTP68dDS/v1J/s2++/to8+WtuLj3bW9xOr67nZcGA2wHs+8m+b1bqGXceP8a8aDPi6yn963fhd/dwAP4M3hOvfdWfPga/H99Pz9rg+lfG5/ffiOflecJ8GuI/+dzpeQf5s+bcuX295Rsr3oSvk1+jU8d6ejkfrcLHMX0v5+nFDBr3hf7jG+NZgUNl0Orabm2t79913bL6YW9OMrO9xvXTNer72kb+nizPLv8S7rPfy6//xF/rFdGLTprYBLubxaPf3K1uvt7be7K09dnbozE7+wEnvcSEAkB4A6ayqOhtWZsMEjjicfqiIK7+QBIdjTG+bIIkLzs/9UU9rGy8mdnW1tPF0bIPRKAFk8DaA8BL78/FmASxvAsSqAIc+4qKccCAAjsHAbzC+jotK2yF04Nl5YPC8QzvZyTpB6wyQ/Ju4HgCKGwv8WIDN+iFv/PHU8veP3dE++PDOXr7a2uuXe+v6xobDqQ5ghQN40kHoh+lAxsHk4cXr8YOM/84H2UH9HQ5tHOgAQn43eGpdvwCW8Ob3CZbRDV/5er4ngOAp8dydDADAgQfOG54bhz8BnAdd94uYwq+djnbqZKzm85m99947dvPoxq6XSxuOBPLj8QBTYcMahkb3I4wrwSpYBD6s+ut/5ov91Wxik9GQBxwnAd5jvdna/f3OdoeT7Y8dAQKUdicHCDxHJ+8xtJ4ehK+VINEFDUsEEAAg8TU6IDzwpgIgAQwcKP98BIDMJza/WthkMrFRUztAehsM8VflQcJihfUqPcglQPQz7qRolXTjAYSuP/Fi8YLxUAA0J/cA+CldZD4/f3Fg3cBfr7/f0wmWWWCClyu9iTyJbsCgH/K1b9utHY6tHY4He/FyZbe3e7u7O1jX1VYNJwQHfXB/dMMCz5Q9SDqU7kFK6xyHGtcKB/bSg5Qgiu+XP+NHRRcsvH/yXAGQfK9Lj/bdAoTWn64cRtXBAQ9yOLo1DW+mvxu+sxrpveF9t+3e9u3ODofW6npojx8/smfPntm7775rk+nY6rq2/X7Pw1834+RBR6Oavx9nCZ8rUumt+o0/+0V6kPFwaCNcROttu93ZZrOz27u1bfdH2x1Pdjz2AglCA56Ak1UMs0qAABwCiS6og4SuWDdWBsJDlvAgAAletRAmy4vHuLZmNrb5YmGT2dTqcZNf+FDhBFCfgaHfxbuQsw6LE8+pWx4A4WVOkZhO9dnrp42K1xrA13MzJsNndWPVqLYOIQHClgQQ90TuwU5dbwDP8XC0Ax77k+33rW12G1o93JzV+mDb7cm2W8C1tmqA9wurRlMgT4Qr7hY2whMezPAiRYgb9wGAPwMADlSgxy3GwH8PPyfPp3/yTtkj4XUmD+LeNp7qEnDl1+M+pef0b+bfcavi11DeAyHW6U2AJI9WWTUcuFFDGHXitWzblt65rkd2fb2058+f2/XNtV1dLWw0HCWvo3PY26gOgOSzFGe1+h//7Bf6q+nU6sHAalhPIBE3bruz17f3ttm1BAlykeMJHsQPPUMsAQRHBiGWX9sU/0fOwUM78PCIB64ACA4aEOs3ipY7blAzstF0bDMHyHg68fwlXCsOi4c3+J0+g0JhlQPEE404MDpL+F7YoQAGfUYRqzp+Ij/y52Q+4R5w2MxsUI/teDpZB4t36pWrhJf013Q8ngiM3U7GZ32/s+12a9vdRh7KejscYcUGdjzBTOFG1gSdAKD3iVwtPEiZRwR4Sg/yEEDCQ5YhVfzOQx9L74G/C4Dg9aZQEzlScdjD05znovJ48a8M43L+cqSX7GFgmHvEg6ELr7fyIR20AC68+Gg0sqZBSIow0+xwONh+v+O1nkwa5iPwJE+fPrXr6xsC59gdUhg6rEcMKZJBL4xD9TcSQCqCZDio7NAebbvb2ysk69s9QXI4AiD+WvleHSCIDaueAKGjYI6rBJyRPW/uwKohEM6IPnsQDzf8vLnnEED4aIY2nIxtNp/bZDazyXyaACJ/HH9HBh3nnQ8P85QHKKaiTeTXi0ScoNTRihARYZastqf39GoCG+8TD7wSYoRPJxvZqRryBcAinY6dAQz4iIN0hNc4nmy/29tut6fxgec47jvbtwfb7bcp3Oz7gXX9wE4dngufg5SQB9GBkAeJQxiHK6xdfK90Dvi8DK/weXiUMpmOnym/l0KmNzxICZCHiYFPCpDkBTsA5CBP7OAAULpj5zmve3+GipFz4eOQAEH4RKPnHA+AvNtteSmQpF9fX9vNzY299957tlgsbFTr2qYIBM/LMxpRj0cx/9MvfrFfTCZWD4whFpiowxFPvrfXtytbbXa2IkDAKiDMGlgfLBZiNB4uHTCChCFOBgcvMlidgXIGgiSYFTBZnmDpvnsYE269Htpg0th0NrfpfGazxZwAQTKdwrGUy/hZd4BGmFWGbMkz8bwHWML29iSM+Np4DAt2g1k8Dq0AAjMABgsXdHcwa4/KpxRCyVPgwesFz+HXEyCB+8fPWDew47GzXQvGKjwqPAeAITACKPItOuQyS/lfyvE8dC3BU4YyZT4QB/cSNPhvHDQ8AiQ5adXfLHMfopWH6pMDJF5vvBe+H/eQfXcw6w/OXMnAwJv0DOn8+rg35vvB9XCgwFABIPGvbsBQma61s2Dj8dgWi7l96lOfskePH9nVcsGzKgIGRFPFxF2GMDNm1d/6T77Yzydjq6vKRoOKIRZu4G6/t7v7td2v93a/3dvhAMZFlC9CCDxzFQARM2qjnEIk75E8CEKsFF65Z4kbTnB4bJ8ObmWn0ciqcUNw4DFfXuUcJiX1OjQBPj++ykNSPiMmqfQgYj9wIQSQ+CemxJPxSBp5OmgKDFae9LV7v/Wut+1e1yu8AzywbgxCC/0uwIOHbtrJRsOG39+1rQ4FDoN7RNHgmQFkUj8QxY2Q6/KfPEiRuBZ0e0lglB6nDKci7IpQBYcNn/M9ONUbfzOHSt87QErvkmje/mBVD6OC3EO5HAFCAGW6XIZLhAPu54kBisAd728yGdND4L/xHpCY4+8gBHvy5Kk9ffbU3vu+d0n8IDRTXiU2i6FWyl3Nqr/953+kn48bqx0cOKZIJHf71lbrrQCy2dGD8OFsFmNChFcMsTwHCSYiHTgAofOiwygd4nSj/OcYHgVt6DEQPhxHI7Px2KbTKb3H/HpJw0Urz7zAOX23sgQj2COyTAqvxKg5lZxYNg+znOplmOU34ZLu1FNGOIeDbjzg8BBte7S7dWvrzZE5SBx0JurI1/xaRS0EX4sYnjlZ1/N59Ls4+FETci8LgLn1FLOiMC68w6UHAaguPURY6xIc8fvxvQBReJAACMGAiKAgTsKaKx8YMkmOf+XreSjEiu8/BJC+b806GQuEVQSI1wOiBpXB6YzcoLKj3nQCDF5qjZCrqelV8DvISQAU/H0AYnm9tHdBAd/c2HK5tEGcJVzrYCmH/jf+zl/40X7W1NYM/ELwph0YH6MOsvJHezixJoIHGBkCBK4JCXok6cmDyCo7ZyXvUKnAVYIj222/xB5nB4t1RK1jPLYJADKf2+JmSVdFCxuAcvvP3Ca9HhXy/CQFpytA0UM4tlLS4kk1z0NZFNPXBbAhPScOdLB8oMNfvl7b3ao1UIU5fh+Y0dOI9UvAoStnlqYCKuhM9yy8pvpTqXCIGBlgTMwYX7gKomfgYMia/SC+j8Me+cZDRcF4raU3YpjhNHeEWlETKsOiCIUBjgixHgJgJOPnoVkGcRlidd3eutPOwaH3LCOsiAVgoaX3i0T6HOlAYvlULKYXQDm1rm0+n/MahCfBuUbi3owbe/L0sb3zzjv2/Pk7NpvOSMnj+/hZXrd6xBC6+l9+6cf7aTOyBm8WB9gRt98fbLMTSO7XOwfHyfatbqrcwScDCEITM7nAiGXTBXU+MTEbEWJZZcd6ZH0zpisEk3X16JpxrwCSC2BeKTgHSBR7EgPlBZCHAOKVfcT8keQhx5AXqugNIo9Yr3esEyFH221RSB3Y4RhW1FkzD8GYpHvSrhqSLHKqbDm9HYVIFgRFYzCBPHUnHgyRAw5eVMvP1Aq5Wp5AViTmuNmXACkp34cONg5ZZoaQa+Vag7xPWPDsQX7LADnt7ESACBzKcwUQFQ5Fk4dHjxDrgMKgAzs8IN4vvi9mS69dxgjU+p7Amk4ndn1zY48eP7bnz54zcQ9w0dCyFja06u/9l3+on9QjehDQtogh4D3a/cG2ewCktXuvqO+Rm7SIET0e8cg30bwXOUj2IggdwPToK/FRsXN4jwiz8scD4spmbOOJioXLGwBkyMQesacYYoUl8iB6NoVYwVqpkh7+LCkkvBDInNvDKDBHwRghBAqrifxrt2vtfrW2u/uVbWAw9nDbJ+urMSlZ3bxIJj1mRtJOVkaMFnO35CzlKkheRIzNgqAKlmRYKI0oKvgAbEhqgs3CK/ZQTADRayhzDL4u/sy5FITvL0iTIrcZOtPFAwZPdFFoTHIaJMsXIZbIS4U9UUu5BE+EeMlYUvbS0oOA1MgAyQwMvRAo9HC0vO/wIAIImSz34kEujOBRSDpIloTnoJcAAAZm0+mMNbZPfer77PHjxzafzW0wFJh4JfEe/v4v/2Q/GQ2srjqrTkfrEVsfQEUebL1FIeuoYiEOyeFkmz2SdS+qRSjPsyg/wSiGlyZXqXPsUKbDAodHMOU3ogZnB2iv6sYmE+UgQPxgpCo3azKk9AaSsQQB4BYuLPUIB70aUIaA4hGKSaPRkBcTIAtmilZxGFKW3oYo/vVgqBBSIc+AsmDNvOywRzwLrI5s3/bWHhXe8cHD6KFZyl/KN5ppaIZaTisqoUeNAc8DgLiUIrFpAJNCRPfDIg74+y6H8RBWIPFCpucQOK5nhcWM1CAQndguQAyWymsPBAk8y3CUDt0Q96LQwIWxi0ghDqWfzrMIIv0VsSuqgUAt4HUQEgSsLR1F3ZOoAPXiNSi/Pkd6XV1TBQ1Bw8PLiWnEaw6vqbBOEp8IRZ89f2ZPnzxhnWQ6m9pohKgA56Wz6n//lZ/spyPQuwiwWzu1ezu0ykEAjM0eAIEk4mi79mSbVlYxwmVZKk9j/WMKOPSaHR9ntdszcJxd0MKjtGBuRjUBAtHZ8uaGNwdW9xi6GfxMsFie+aQkowczNyQl251gOQCQI10pLA4ZOR4kgQ4MVujEEDy2x5NtdwDHzlarrW22qGVABoJzinh3RI8KzyoZSYj2imJoSi3co5XFy7g6DhLJVJzRKpLxdHMdHMkYJCFkWNZsYYsrr0P+AEAirA1vcxkCh6VnEu/5BnIt5luwzPAubrDKxLsMowMcJVjiOsUZ0mGAUVChEKEQAALdVHc8eFqm/AgQwfkKY3SCkLNgIR1KXqeS0RvCSMKbuNxG4kZRyngtoH9RcWch8WZJHVcKK3/zV/5wP22G1gx6O+13dthuCBAk5ftDR4DAi2z2BwJke1BFHf8kT4jzn7ger4Mkkskt1HcPkD0s/6ixKQAyB0CuxVWjQAeAMEoBQJz98RBO+iWSo6pXgLqDd+yhCkV8KmsEgEA0OBzWrirG9wUSHHoAAvnXCo8VND7SpA0qaazw2KHmQYZE1yGDRHWjRGO6lSsPIw9eKJjxd0PBWsg5ygIeXD7zL5eaRPKrv5GO21l4lQ5oFMCSvdL9eKgmUb7m+BkakOGAyS8MQwZIYZ2L3OjSe7wNIMwz+E0BRIVC0eQAyAkAgbe2yhAyBdhJ/0J9AHgkRUSqAnvVXV6ErzflUbhvCMmg3WoZcuFMIJwEq/Xs+VN7/vwZc5LpbGbVb379J/opcxCzw25j++3aji0KXSfKSwCQ1eZgW1R9W4Cm4iFRjBkHMcDgYZbuSgaPTFH4Bv+uHyiVCfxfkZOYWQvLP2yUgySANHTr5wDJ6mCGFwMv9MEzACYIwSQik7fzG4mknJXYYcPXB5kJ4ngA4X69sbvVlmHmFmHmDiAgX5eq6tCnMV8oynelgqD0KmdvHn+JgNVrEbXsWrWgdlNiHgVLiSIp7oniWhItZpVwSfMGdc7DRMl4GDP8zSzplyKhCBP5/KKd9XwqzAEg4X0ZutCLwJvkRF53Pm5nJmUuAZLkA+lHodpVWCO9GnK8gx0P7RsAoVHxkPpANXX8xQgrXW3N8BAgySDGe4EngZhRnupAMgT3AGQQBI5Pnj7hR4T01f/xl/5QDyUv6iDtdmO7jQACbh7SEjBZq03LUAI5CFgbsj3FoQ7cBiiikp5uiFvPN2jdoGjjufxjuPrWRg6QsU3ncIPXNmwayt4JEPy+q3oVs7t0hC8Ix2Po+qxg3Lww6AkkPYgh3Grc0IIz72y3P9ir2zt7fY+eGIRQyDNgbSFqkxwBP7ffHawaZp2UH78zW/AGc8eXgjrKnodQABFjppAn8govFHpizeKkq6LP6PKCGJAdygoAqY5BKYf3LAESxVBd9BIgEXokcBAgel6GKp78noVbNUIZZ/0uipnlNWC+oT8Y1V3/++ceRIoDWHgARAaP4XKokh2JRztalzwIr0BWZHiOWiGKKFoB4ImQg6qVQSEdXhN+Bp4DtRF4kmfPnlr1D77+Ez0oXshE2t3W9tutHQ+H5EHQNLXZtgw5EHIdTvIgpT9wLa6Oe8o7Mhx0XnOlt7SmZKDeCpDaukFtzRh6rJldXV/bCMwKCkDeeNV7XSQn/O65vN4gAaNK07S/A2dY8HudahIACftdoJliztXa7d2KRdId3jN1aLBGIWpTZRyKA3iPvoJ30r2JZJKHK/oX3DqrSCj61ti3gNcaBzV6THQ1lehHYVEsGNXUiQX0E1ISAbywTlWnYg+YKoWWpQcpgRQAufQiZz8TwPNDSk9S1zxoo3rEz1WJDknOuTkkSMhnFwBJ3sP7XTzEQnil8Ke1Q7v3e+d1DoRMZeMXxKUFQCLQlwEPY5GpcZJIkKagCC1oZno3aiDDIcWN77z7jlX/8Otf7kc4TH1nB8gldoi1ARCwWT2tKarqe4RdOCi4SSkmSrzFGWAiIE+QiEr5WTgVLy8DJIda+l7bj6yraqsb6LFmdnVzTck7bkYUC3vop1xC78ajOCau6HWmJIVYSDhHsKxoWjJrD521EA9SMqL3C5FmUjF3xvcdVjwZQN5vhGUKX8rwJmuddC5ULJSsHYl46p0pkvZorsIbCpkKf4cScNG0IdHORiZbl+zVS4uDQxI0RvFbhaeJr5b1ElXRwxDmRqVoFoMnwSEDOOq6UW5CAkV1qgg141rRgngj0EPfIxMXIdYFQJC48yAzn0BbhoqEyt6TaQwLlQSd4ZHjYxxShGgID+OeRVGVUh5XRMCLgPqt/s9f+SNk/0GngduHnghxWetSClhUgKNtD+L0zyxWZk9Kr5ABIoTqvYSM4uwnVcOI0Oo8TbG2E0DQKIVq+vL62mrobNAXomdGwfoMIIkCZbTlQjrqmiKo73gj8Zx9BzqvF0O1Pth6LYCA2oWEgQ/qz8xQ+qEkhOGD8gbcrPAKpbW9lKtEkSouPq5AThpz3ccbNVN1XXKVXA8p20bPr6L+Kx2EC/9+qTcrwRxMFr6WqGrPQSIjDPlp6YH4/guANM1Y1XuEvGzD1b0/D68kTzr7WvIiypOi6g0P0pJR3autupeYNjW0Rf+KnENhFCWAPc95y4Mlrx2V+PJesOqeBJRQAY+t+ke/8kd4HRBW8UXtDmRlpDUKoJySyE69456I+UXIViKZjnjb/sJdAh80bJHIRZu7Lqf+hSc5ECBiTAIgzXSixim4copsETh533W8LoYhNC+qxDLuV5sw4k6GBuwoq2zfnuzVqztbrxBKmjwlYlIwaH1lBx5QgRBdfwdPrqk/AxPmoVAOlfLNOGe2zq03QZRYLw+nopZCuje0XGXvuev5vQagsMWLrzm4SqGFQgyXbxdU6Fme4gctwqsAvDR0cc4iy8x6Fia7nouAAZIXkTyj9+Jhou/jdUaY5e9bNyYS+dwzjhCLUUwL9bMAgucKmhaeBOEryJcRe0AiOY+2BhnP87AmrgPNQxHaem7FOljkKriv6FQdWPWPfvmnerhuUrsIpXYSdoHFAkACLOxhOJ6kvUm+POLJ/McfKhLhjeDNReW8dIpvAwi+jsahk4mmm0wn9CBomoIX6WClwnswwpZlT0CLvo1obqLlAkBONsLNbMYkG1Ah/+CDF7baQEEwJL0NrzEYIoyrBBYHL2LiFtw88geX2ZC3o0WKQySWSY/cYsvQYCiVLKwXbuGlZ4nDGaI8qYb111mcRiEd0XBSRcffiFfoCWq0BvvhfxvVHIcss1hZcIm/K1VLDq/Cy0R1F++DCS8NTqP3htAl4nsm17p4JbEgXBQ5SnqPMgZRBwE42j0kKMrdSolMsH818mdvBcikgjxITgAu2TThsiweKipQ8k6RYxRq/+Ff/HIP7hkeA9XzAAISUPx35CP4iPBCDe4+iCBquqUlCxVKcRFKgPAiP+BBypSOZzoA0stSASBI0gEQeJFzgHixKLFAeBEaqkCbknlHxvFkYkY124hRCPzgwxe23UGpPCJzJ6nZgCEWFMxRd4BV4c3CAAX3SFKzu9Az5OqpToE3It2SdEIqskkbpHwkDgNyDUpBSplGmqgiqQhOm9QhZUGyBEkOs0qPxp8vmaULOXz4tvMQC6K9surv3j1CJLbk4HB6sg6QoGkJ4HDBYAmOAMUb4VUCihswJzRkpFsCJLo1I3RNH+lBlLTrgBcMXvQUpbbpIrSj7AfJueojeVRD0XwVUqV/8F/8wR65RYRUAAUEdvxvz0ngSZiIchCBqs4R6wsbZVwperK0EkA2iQAPsd4GkDJJF0Cg99cBQ4h1db208WxKgDDOhQdx+XtMJKHlYKIEVmiUZAoRayt00EkDVQuG7qOPXtm2PdnB0HOh0ApeBGBRm7EL5egRXArCxPnI16DKaIRy0b/N6FbFSi9WhffAazmd5IlS8u7TOM5vdMhIzltWw5IrLIqDJeAEe5MGTGiKztn9iPpLXJN8bQJ4MUWkBEimg1PDHN8bwC9w8P1hWgiSdWfcLwFxSfnmwNNbIzwHSABBiOWyD/xsmd/xXOHaFjR0risJLCH/DDlOYtNwlj3pPzGykEQlBI9Rm6p+85d+tIdSFeET+hv26E8AQPD5DposuTlaYrbkolurqHQUbbQRUl4ChLbc3WCIBiMoiBCL/51yCJ2341E9EJCWwHMAIJP5jABBvQ4sLcAGcMD6ahIJDh+SBlwg3CjNOIkLS97bBXq7Parlrb16eWtr9N6Tv1ZXH5J3dE9Chay+Do+XXXYtRupgFbrE8FARhm8iCmtDr5uQoPDRQQx3WMOBtFqmIsIoGS23gt7ucjbP6azXvZSzRCU9xJLOObtJijpL5BnhgfiKL9is8wMd4kav0VB6Hk1MQh3e6yVABo3qSmyzSTlGNqI57ziLG1JcEaEnPUi7V+OWzz8LAMRrH7lSItVpXCUR70sAOQ/xENHU0QIOQyeJKgEjY6ZiMJ/j7/+FL/VsESW1BsYK1cvcENQCIG0reo0uSRMgdGPFsLAm4fE2PYq7iPAsCrHEKHlEneJDhSjeOZKHhTibgFBEFWQMjlssr2y6mBMkBpoWA9d8nlUJEBXNlaQTHFEN9hZLAqSDnKRjiAUPgqawDZSeQF41kEwELFgFASMOBjoo1ZdAER3DrSOBqoavXM8IGQoquKJuJZlX74d+t0LtpFQbSJGXqdWIT7yYxcPNlufsrZL1LeJ5HX73GF4zST0oqR/nPAdIzWrxhBdRQfJKeE1B1fpHOuNCegJ2sJlMw2UlWjqzWgWRk19o6LG9UKqWiuMRAIm2WbVWnIEah5oD/EQclSFWyglL4sb/dJzHZDBYUFQOQmlSkU9Wf+8//dEe+QUZrH3LpJVdbu5VpIk5UqvCZGzUiBmhLAPSAG+RjL6Fkw5DhFM6BKjAZslxdqsqfOmoZA8SOQjrFBhi0HekZWdXC3YV4mOFXpEBZjuG8jWeFUG66hvgz8NV6vUINJrqKEUACqHffv99Vs1XOwBEPoehmnsTzyGVbMKCOquC5xT1K44+rE/Mw1LyG01TXuzj7yKXw2SWuOEyt7oGJBqTd5ECWSGPeiIuquUP1DMuwxrMMkspSHi6YuBFCg/T9wqLG5fVAUwfzZcoS0gvSEpvaMN6yKLufDbjQVdPi1hAEViOXBqbhGIP/2jqUihIyYnXQ2CQzpL6RGmjk1WK4xI4+XNF05ehJL6vZklnsLzwWOY46W3/b38eHkTJOIYKIEln/zmBodALMWBQeZohpMqnACL1abSY9hwsV7hWTl487zy7BEjkJGk8KQ8nrn8ApBdAFnObXV/Z7GpuA4yTpOCwkxrHR0NJAq7uP1r9RGO6che0MKeGYDhFT0Hit9//0FZQ6h4lE0/DDzg30tk39uDnsatxwwAOeFAChAmj3DMPCL2NDnbOkdULD1Ux+yLdG0cfVTAKDAw8dJQkQjJ6hpxOYZ5/zP0XydT6GUQ/SrT0plDQheGZUSyLiyVAsj4uanNqxPRQAZEDx2o6QJqGHXp4DdFDH23DNFLpd0tPgq9/PECC2bs87CqlmIMAACAASURBVCVAzryLC2l1/7Pn8c9SzliydGX+kp7rf/1zP5IBQpoXMZ+AAZBoBIvUjkzEak2MoPIS4IgeBhwEsp+FligIUtcb6QKVcac8B7Q0qox78S/m9XLMJqjenvISDm64Xth0ubABZhkNHSBOycWbRwGQIVb6W/GaNC6GspKT2W5/pJSdLBb0ZwSD/6y39aYZwgSIXF2yfl7BJ2UYTUVozXUZOq5dJPmqXcmT4vqdTq3UxelGOnueyij6Y9E4pWsnD5j+lZcyaH+XtudkXTUuDmArpQYeY+uvBEVdmC4WvpUxeseFF3xjFkERbjlABvVQqgcPsfB7CGeVc8XtkJGJa5i9nXp14owAEGG4WQcpBgQm647QnYP5MgDerPEUhcTkaXxmQWmp3/Ycf/cXv0iAaPpDAMSB4f3ScHGqlAIgoHmFeIZXlx4E1vkiB+GLjhzkQYB4NTylaYUH4Xjsnvw6pitisskMABk3nBQBD6I0K102608Is3TTE9PD2B1FJTQ5YYDbwVbbHQHy+u6eyuV+ACGiW37w8ZFY+5MXLezJbZ8lDdH8FE1TKamOEMBfA68HwBHjhcTb86fibntcF9ISHtKzECgXAMtLenlA8JRkgSDJz1qUombkiV/UDQpKnNeO7b/hnsXLEaYeZvGAEyADwxhQaOWm42kqJtPzRXXawzoZmgix9BFnqQRI0N8sL7wFIPg9sKOlV7n8XF7B60PJk6B0oGa1AGQA9Y3f/5//zOd7WBjGezHUDJ4jpkt4I1DobVS5lAYJTSecgsfY2HunCZDzoQKykv7qcjFWL86lJqGtiq/RRSPE4oBoAERM1gwAubqy0aQhe3R0kkDm3bvNwD6R5vV+bG/VhDeCvB1h1Ypy9g0/hziReRAAwjBGWihYP1XKIzRwG+wtraVLj5+JJFkXPFiH5BYSOzJADpJumJCR669ZwhOSeV0ppy55Y3ODViTA8RyXzBTpaBY8XYYRtiTJhjysLKryUt2KA6IX4O+gssS/4hIoZ80kKxBA6tom40ku1BVD5xIu4pPCAwIgaGorPUhU1D8OIJc5SHlPHjIWQaNH/3kweg/lOPz9v/0f/X4CBAc9PEgUsEituSUOnhvtiMhB1B4KLl/6JE7vCHAgxCnaP3VtPTQoLkrEv6xleEhzDhDM0soAqaHqdQ9STzETF8U89x+BP+YJ6kMnQMge6XDp4Fd2t1pTzn53j+ELrWQllK5kOhaHDr9WAkShQbZ+l/FrhDC5rlD4cL+O6RCjZ6XETenuk6gvxv5oiIFodmcDI6x8qBEqjVHyJyWtp3sZTkgYE72pvMv/O0DiIBSJkmdTISFOyyXCeARAhgMCBOG4Rsg62ZFo1sht9LcSUece5CGAcM7uRZKejK1Xmi7vw0N5RQCHzCYZq3zxS9o73wbXEP6t/+Bf7YNnltREQ7ZEZYJa8xI88g9SveT6xGKxz9snCLJW4AnwGUAkAVErVZ5+GOCgB3EhYyTrmu6O5xNA+Az420jUlws+6tmYFusIoJ6FJiAJvPrhkndORYQi9yhaF1J2AGS93VFaghEv+JuaRaELx1CLe1HkPTgQIs1piqZzD3N86JikC5GkZ88TFz1TwQix3gIQd0XJshXNUaxcgw0skRV1hhJgF1+rmBx6fO8/VxpxYaGQZiTwcVKxwli/Oero83E8wU7hC9xjorUCAAgZIU4/9LlkDpIUMkZOFwVWnKULDxI5SJzFfMgzM8Wr7QzHOTByv8ubXz9nt8pLVygCdV7/5s//SwQIbkjrs2OjmEX9CwpBrrdhyyMpZzEOAoiGOMDZoGYRDFGm9WB93kwCw2rxIDpXncByBhCABACpmJiDwQJAmtnUqlq96ayDpkODSjremi4QmpzQIoswEN4C41Rv7+4JEhZF+55DxmDP8Hl4AdYuHCQBkDTpjxNMJHykFBsTwlkLUPsuX0vqF48K9zmlSIVtQUGWliviPOYAOXYzNP5I6lPG1ClpOVPQwuOoDoXDjMEHBUDcc9AIFclz8i5JOyWalmGs2wRvINC8qjAeVNRA2SCFL/LVUDvHsGldzxiI5wJMH8ZAA4qZAagrOZA+SQ7CGoyHfBF+RguxQJGXKp2DS3EKf0bfKPKYiGH82v3Gv/cv9lxSAoDsUQvZe7VUhUC4dE6q84aYIWMMDT9ggg4LzvE2akASQErrmd1zPgTu1iMGTiFWuS8EgINoDHSubgBAghoIqF4UC+FBDqgT4M2Gy/QwSjwxeo8xG6khdY2848OPXnIyCQZys2DoLa9RkS8BQvDG0OpYbcCZTbk2gRiYiaK3sEql65RSGIaYthE3QiZYU7GChix/xsOdVGV3jxBELgESN7eM4lIhMEIz5SkwMZifnP7RDmjapDx5prbx3Mw8ut5bHE700gyjKfD1/8X2MF5DX5DEe+RnxSvSuEcxaDzo3iSe9JdEpQGMLfrPPWd5G0DKg47POQ9atv5MoatcIzRaCdaF/wTFfllTkn3E8wUpUv31P/17e/QA44usmhMgOuH4yAFiLBCiio7OQ3xP7ZGieHXxTtBNnaIIV3aVhb6ndGRek3CAxI3iIaW311vuegCkFkCk+0vFwskCawdGkp9rpHv6A6mS3qP3GO6+Zp/H3d3Kvv3tD20HtQDeI5iM6IBLkgRd7IjNY9ynwk55DMXEsoJj6JDSAOdItP0QRQU/wHMGEJw2BJ7ReahKMDw2ics0eKJUf/qNC6tZ5DBlsqmaiShifM7B5LzrHj661Yx5vgGSoNHoybuOHhfqZUYIZCudKHDjk7oecd8cJMyThuoujKknDL/cU5UDJ3SlBToSCcfsQaJQeMlivQEQYV0Q8TGzMjpFXzqp4IhilCLgDJ8BpBgoGCCl6fj1f/eH6UHwBAEQtkUyrujpOTAZm91juIFSP6mCXdC8CLEIEHgRj2dF8cHiupYpHeGHABIjRaOqDo8EyflI40bpZXqGV/Pl0iZXAMilB1Fow8ng1GIhJBwRWRiAB4C8/8GHBAcsXQBE/e2ayypjHQm967x8sl8KsYq8YAzvyg63uN3FbfepJQo3Yk6TS+MHMUBCrCDAgdUTlEFEIdCtGW26J82lmcmJtk5fhGPnYDGrsZ7CIweFsZGc++fsd4ncy2lZRhRoHsPwDoTR3iymJhzlLH5dcKyTYfPQ5mwsEACSCBAfIVp4RR7ItwDkOyfppW0svUgAJINDoSmHBnlOlhcmEVTMC7PV4YCPv/ZzP/QmQCKDc4Bg2kMGCC4FpNmqpIdloQeJJN3n0gZNSLFfHqohCx19Gj5YWh3jhRSeBT0EBzjI+h6SxZCbTJdz9yDqSY4QC9f9yAnbWtoDoCA5v19tOa3+5atbslbDGk1XssjMcRjQysrw9ZLidf2UHwSnwQqGzrhTJZaXZuvmHsT1PQAHVaIur2auglzb+y3IDLHXQwBJiTABIjuPQ18mm294jAiv3FSXgILIhx0oIZMnQ+cVftZ7FD5FjhAhFifbQMTKgqcAogKsYmL8DvVlPBG50Es6mJV1qHvVZQiApMo6afdiKQ4AAu/hgkTVrj6J1KScoiMP8lCPPwxl5CgRQBmVDAVRwnArh7wR+lbf+NkACObGoaNwLyPlHgSMBAHCaRawbr4JqEds6v3SXplmok6Nk08M9Ag3NETZ+mVVoo9SkKDvDYC4ByFA0AJ7Is1LPZZX09X9l3VNuO7HVsQAGB+ABUVBrnJYbe1+s2GxcDTGViiN9zffc4ebmqrgsfzGu/si7CRIin+YZxwbMuIAl5LsaBGFJ45ZsVAjNGOARKFHBkaeNcZJlYy0MmOG5y/nx0YsfznH6uwFUo+Mwp6Hui7UxLUMHZlCZK9n+b4MHNI00R+9GRQP5lGoVSfVNIwIyI0AiISrPmABeSsAgjzEVdcCo/e3hD5T0oLfRoDkSjllTgkgzqTyXEoBKEW1u1fvoAzRI+/j//CzP9zjQuCAQsm7bzFlW5JmJHbTcW1YE10P0dOB54RMW+IxXli/IEjOKeFwy8sRkmm+LNxx9JAoBAnNE5mQRPMWG6NYQKytg3TDwYMQeDKfs1AIkKA3HQwLcxD2QfvQZ7Ca1CyNDJL29Vrh1WazZb85J7yzr8VdMqYDRoiR9v9lD5LiZg89Jf1XvpC3MPhUcPZHiG2K0Aqeo4HK1UfyQ46BJS8aPuBeArU2ejJpnLTeDhdB87xg1eVpIB/2IqHrwMo5WQyNE80rMNcwFi6LkZdAfUcfk6f0sam8p5yg0nEuAXI8eBF6Cu+SjNoSGs7YSxT9+zwPKrjycOEasE9d0zATtZ+482JGGYfEtTkI50Za7fZg0Zr7U4J5UhqNfzzMZ9q0RCOc0b9BL0TxhYan+JfzkVjP52NX/+bP/p5+jzfaV6Q9sdAFLww3ZTTsbT5p7GrWWM0dIABOS/DEFqSiPOAXHqOBCgqYsRUOoNoY9U8AEcWYC1UX6gPrMDWRuw0l0oNwsJliFdtcE04m2FkIiYiap0gaELxRjxnaZnOw+xU2x66YdEauBlBpxL8PGsBrobcI5WweOB2ddjFdPFo1ac1HYtc4PMhDqYaTPgSQIDmwKw+5HAkPTh0fuRIVf1i0MY8QQI4+E9agNEwjD71TXiV+VSFTCY7MDuUqO672iABx2Y1LaMIw0Wu7kaPwkwDReNkdRKxcaBORghtFrsFA+OVexjsv0TKBWWOYhkmWjUs+0UGpVlzSrhxpVExv9/AWs3I1K0wgwHXTrkH1g3BwQ5yeIk/ItPdZfJIM3nlO4flFyn/Olb4PRQDV3/h3frjHmucjBhgcARBUxxXsNMPK5tParmZjrXoGvevV8yzJSPUzWSO4W9ZFpKGBKw+AhC4ne5BckCsK1EkjcwJAKP4T3Yqt7Og1mMywK+TamulYRUaYR0jfIVfwhT3IibASm97jHuNDt6QtlWuE/j+P8ecN9fAiH7oY3hb949Fplw//oEY+47Ui1gDQWefjaYZDG48bm0FDhhVys5k2CTv9GAo+jruhauFEScjp0Hrlu5OGioU+Ny5nVe9SbuLESkH1xpEZdFh0FIByzxEyn1T/1OHHvdMMrs72J1/uAw/i01UQVqWOUzTW4b9BCbMr9aRwS8mV726HN0H+hfqQquux95zCz9+hAKEZx5n4ja99jgA5ECAnulVW0KvexnVFD7KYNqxns0CIgg5DGSVp3haQBrkpngVIfJ0WGSIwCSjG6a4IDEHlqiD3EEDgQU7UU4llwQAHAGQ8nTlA1HrL8Ioe5OhFwyH7zff7zlbwHtj3vsWeRYyYdMs29OKQU4IMl0rdUDQelQfLZ+BGEYxNZGNsM9LgAoBjjOEFHl4BKJNxw+HImNiH8am54sDpC7yIWnkMw4RhzUc7wpKiaAaLfkQBLeLlmBwvsCT90AUoLqUT1amzysPDCLFi4Q6j8dCe+UKfSKYx0SVt73XPgvAKIOJYqAixsD8FXyPj1Vmb6Htc05iwDoCocEdVQlhxp77RHPU7xYOcVd5//d/+TE9goD/C36RGYjpAxo1dTcf0HqzG0kp7G6MzH5FH0u/4RA+FWZKrcK8fFuh8DEB0wwM8sn3wHgQGPZM2stcT9KTPbPFIAxy4f913wIE0wOGAxcIwOO42ud/Z/d2e4FDyqEnlCIviIMVoSiXpRWuoWxG+Lt8IFZYlku9mUtt4ogQcIRT246XxNJURMIurhS2QO82mqXUUBUwO1cYh5BRB9F63fBz2rQCC2gOAwy4p92JMlM+JAkfLWXtreA+GLAQIws9oCMwbqVgIdYCkKSvRnMV80ied+H2V8etoTGOxK7yIto8BNCfbcmZxiI+9kQzv1we+8eX7YVDsj4WmmqT4zzLEint7Vo3/xp/4tEKsvqKbbGPEStUrxJrU9CCof0iuIFGOPId/LCrnmaLzRqrIV5zryTmkL75xOUcARCmr/p0IDgEkPEjdYKpJ9iCo1LJhhyGWBhkDIEjO4T3u77e2ut/5mFGAQw/kCwj/BKiyFzlLQ+KCKSR05gWgYc0CLBQ8x8BGDUKpMVWss+mUeQa+h3AKXgU7FqeTCb0Jw1dKU2rF41izBjX1AaI8hKVHjdtkyNXZCUJShqne4py6Nf0iRUJeTBu5TNLpgTiNxXs8+LNx/SNE1gYsqSN8YB0ERfRwUUl31goAoQdRQo+PNK6YhHPEigwYUT0vV4AP1KpNgPhsgpRPRIiFmWMFQGCAIkn/uBwkGMN8r3Lx8aGcRRlwVN/f7DY8121VVv33f+L7U5JOgNDS4mgKILPxyOYIsSqkyKr+Rj0X00Niq1MKmfziUv4ePdhkocI6R5iVAZLVpJ7Au4EBaOn+IwzA1IkGg+Omtri+tmY+TY1Tmm7ifSEAyO5AcNzdbQkUMFpaqAJwSM0Jj4ObmFtl80rjlLC5rCOHNJrMFx4ErbOgaye+bHQOEoHbUyXuxDA1JuZ4uIdS3K0Qi2wgE+EDPQaScgAmEnPKgHg/3pTshCcJ5oqGpVACxwEJzZzyxjzsQQXR0EfFzsQMBm3tUj1CTJ4AAjAIIMgx8d/hQbSBTDKemAbsO+UdIBJz5n9xnTHNEwBReK8kPQCCZD06Cksg4PMSIDo9RWdl5BEXsukEEP965EGXz8289Btf/UzfIoZEDuJvHAk2rBeGWs/HI1vMGhsOQDMqeY8bImMYcZEf7nDPPk9Kc6SUYOcbqjpIZrE8tIp+Dr9+HPeZkkhU0zFTd2yjZmyLm0c2ns9sCKHhSH0OJ0pgcFVGttlCWrKh99hsWsOEEdCNkjpLZMp+b7JPmlVVNtbgJsUekWBC4vXre1LWNuOBjZsRQYEHuummkzFDK1G9xQ3j8DcvBqpeLqbKJ4UIMWpxFkBgZDzE8muSZRC58BfKB3k5b88twrAEkOTpM0hCrFgydQy1kqFTZ2kABOLQCLHgOdAxSYB4ko59KdglA+Cw3oF3yW24uP55hlUU7IJejd6PEiAli/XdACS8RBz4kskqPVdoVEKwWP5e+t5f+xkk6aJ58eZA7QHNAAhWIiwmYrFA+aIuQgG005IxvaRo50uyEq3SckYEB90H8KY8JCXmUSt2l+xSD7xYMiJRZ+Hof1zoxgZ1Y4tHAMictRBKRgYqJCoABL3b2u3txtaY0L49cgeIhimkNgVfP5ABki+oXK/kIVk2LRZONGTs8RuPRzbBYzqlF8Euk+m4sXGN/X6elPqaZwBSwx3Ur6L/D2GcxHMChRL0+JwhVlFJD4kJafPYbFXQ0ymPou3y3h3XZYV+KpS+SZEbOYZrzkS2KLwS4aLOwmC6mLcytDJ6lBRiHbSNDCACSPj8CLM4ME/vPQc5WeoRw0F+qwDJIZRLGM+kI4V4LdQTZ/7s3LPxub7xMz/UE/2dCSAYN4qtO13H4uACNO98zM8FELVGsgKZMzE9sydfkpb4wALGtWaHYCn950q5eyoUpuRNb4TDo0MSwaYmyE5GVo0aepDJYs4xpBkgfhNde3X7em3bDbj0k9X1WAChyRIJgResbUNOQRbCNwozi7XCknj6m3Dvgu83dWXjZsAFkKxzNI0ePkmRa91c4AhvyueERXWpN0MMxVruwXrfhqXmrBiMIT1myLLz/CtNRQ9BaP48pBzMCf1//LzwIolm9vwqQBHCTIx/IjgYSsXmXQeDrwSH99BDkhQtelULBH4nplRiqAPrIYWwM4p9eF+h3v2dApDkaf67rwIgkgrEFlt4EIAAKlAk6fQgkD5wtwYAIjsdN4FIQ+WaF9+78sBk+HRyrA6gNfG/WjYlyQiG7DrTv/jRqMoTJAjRsGsdh2tU2/zmkU0XC6unY87IwuCHkDEcugE9x+1rFAdPWL3oG1B9cy1DRVG+MRkwtDfyInnntiZJZiGf9Ds6qABRg1Xu9YA1Doo6k/IZYyxrdQB66IObz+TehXuRi2iAQQZIWlxJShThlqrqH6vFii22xa7ECAmPCNtivpaHxZkiVt6SQiyGxnrE+CfJUE66H765Vx4EoRU8N3aoBIuFAiOYLrFcDLU8zFIOWDaU6X0LIDEzLOcg30uI9Vv1IJcMYfVXvvK5voVExAZisXyaCWLhegCpycgWE7BY+Ansj8YbUJleANGxD4sY3XtM4L3dVXodj5n10+d1EK/6++VKhppzlRieIfNBbDRidR1eBACZXC2smU5ZzeZkFFaC4fKN1XMApN33djwo6VPlXGpOhIohB8HXkzjNewTCe8T3NKonU8BU3wIgwwyQmPzCfIZzY5Wox9wmvPPYsacOpMyikE0jDuFBVDVmOOIM1mU8fZaglx6keI3xM22HxjBR4PxaqLXDY0eB1D3dJUCYg3iVXSshgrHqrPX/jhIBQLM/YhI+AKJQi5u8nMEKgOjYZLlH7EH/Z+1B3gDIr/1bP9gj/0D4osOomBWQQe8HVkRPmqFVkD8AJHWEJ5JIvAEQ/xKXKEG1SQuDECsfrui14CVyj6PPXZflruZtAOkJkBubLq5sPJ9q/E8CCKyaMTm/vV0TIKejAIIQC6wSJ7L0R4Em1npFfSR5B3gA7+ArPEiI2yKJH48qehBQuQkg8RwY7Ox95EoUy66E+DxryCTMljUnAJWUJCsbFHp5E8sDX4ImEQpcRASARE2qAEiExYUH6dyD0GtwPlrUQeRB6C3ckAIMzD+c6kUdBPITrKsLFTDrYYnu1dpoGdcUa/O/gyF7G0BCanLpRb8Tzfug1OSB/OMSGOn6/epP/0B/RCFvAOXs0LvMJA8EQGAhQff2pz1bN4cjrTFLFqD0ICFh58A0tb4iPqYUoWBVuKEqgSHPoaJxi+tGwEgMSRk9QizosiA9GQxtdn1j06sFE3X2prM2Iw4fN04eZG1Mp07KM9hTz9H8Agjms0YIRavv1fWolAsgPlYm5SA+Zd2ZrGZUWQMjMoHOSrPDQkAXU+0FQt+268MKonJPeYtPi1c+Is8sYaJSeMmwNaFFOrmLSnp4hKK6XgLk0B3t8IAH4bktvI82zMYQjqjw50IhchQKEyMpd6lJYrE8cRdAFHKxlwSWj7kfAOI96gn73hThW7XwuoMlLOsgIf/5rgBS9OhcMlVkAzNSBdoztkmmqfq1n/7n+xa6pR7xOai4THlCpYz96Qi1utOeSt7hIITNcpF6eyHscafCEF+j1wEQ5AZY16vQKrxGdO3pvxmnh9P1z9XqiUYmVNSRZ7g2C8Os5wubLDDAYckxpKq262bsjie7W+3s9vXGOrwOQ3jVaBI4X4XCRO3+RlUdIZEo11KIiPqGuvzw0D3mf6OW4tuOMMQCk15y16WWWcL6E2gQLEKLlAY68LKrbyFZUlGzwc/HIdFNzRPbKRItRHup/lHkHbrn52oA9u24AvuM4eLPSbktdkoUs6ba+BT7yD98Rhp0WEHtxgR8JumUnZxYKtge4mdOLDwrYffkzpXUIkuy4JIRRzEOFO8jqN8oFKbrUjBTUfWOEDRykNyS7MxVTHpxKMSR5X8+9C/4mF/96X+ubw8omkGaDGpSbA/mXxEgg44ggYrXuhZ9h66lZz39OwIEuCBAkLd4Eh/DECLEirGcAbawfnLPsD4AhsaAUt1bDa12VS9CLQAE158cfYdd7gII6iCYrVVVDWcKD6EL4n7Ant4RchOAA+ODJC3HzFxRu2KxAiCqmwgc+HoBkJG2HLGhjKuRfWE9ioksKA7kqXz5pKxJDBQoQo0YeBpMRtxIz0sUh5zv+XgIIGe5iYOFI5ouAKKfE5CiHwSMVRxGjXVSWCbP5ZQvx5hqNR1yvVgRAZDg6wDIGjRv5CpI1vlQSwUuJKdQFluoxJoJIOWBvwRIeY7Dk5QepQRJ/OxDIdZlOPVWjMBo/dc//em+PeBw4YWPCRDcZAJkhI45zJ2FlgfaIEjdt94/Bv8hgCRQugtASOOd/IyZkgcJgLBoqMMRsvlwcbxtHkJ8LEAgWlzMbX59Y4NGSz0ZHwMg7ZEbalFJD4AgYeaQMdQiqEzBMDqc+ooAYcIca75YaRcQQtxIgDDXzPURhGeYgAoKHLkNwjf27vtAZQFEW5gEELT/KhNhK7CPB0h055lZS+aiECXmsacPJenhHS5BElRx6V2ipz7yGtKsZJ5UtdbaMwDEK+g+hjY8CIBBJS9AQrm7quyQLQEgbLDi14/0Ir9rAfJf/dHP9Fzt3ENbNLYR6wXwHngIHPAi1kH+ABHd1tiuyDTiYYCQBdaoC+bx8AIHb7GUdD3PwC0noLzhQbyiCwZEYvtKPSLV0EaTiY1ncybr2EfBuVbYP37qbLM/2mqD6YnoL1AvCg4nByI4QBAawYNoMrmCG7DYBEIA5CwvcQ9C9ipX2ZtRT5BgYBo8SAaIwiuGWKynyIuIudKeFY7t93+53p4te3mIyxAsJCpndYxS0XuRo+Q+mZy7CEy590UAyR4k1jTIg0DirvBLYVU85EFUKJQHARO6wepsD9lUTESo5TIgvHcUUGNfjPe0/P/hQd4aTj3wjZTD/eWf+mwP7wEWC+Dgg2EEyCFQj7C46gOhBznt6EGkVi94Qpkn9cM7OPS55NSoZWTVbzEkOgno4tnygAJYIDEgAAU4NFXTEW4NxwLI7Hppw/GYNCIAApp6zaHUBw6JA0DMMBvLK9cdAF9ZTe+hPhJOJPWhz/QSXh9hjhKDFLyHJINDIKlHnTU11vKp54F958xBPFQDk8WwS4xW5B8jzuuKGVf+9z2Zi2nuEd6EZ4gVIuEJvhNAovMwPL3kGrnISBOXVLox0V9tCqjFpGq61yhY7XYghAcRKHwbMLoQT71tkaR7Sy8BgoTdR5+mEMsBwq7T38YQ6+NCq08CkDdo3r/0k5/tseYMNQYMMsCCS8bZGCKARLbCzHOI6CA/wexUPGL5i29d9Uqw4mQv9jk4AJY04zYo4HKK+hlA8rAz3DwkdgAFFKHwOtQSO0AGzZiq3tn1tY0mY04pQYgFgKx2R2qxMOpHHdnaNIUDNmSnZAEQaLicltWinRIgolrTEMc2DAAAIABJREFUxBHmITr4kZMAHKimM8RiDqIQqwQIwyyvIqs7GDmMQHs2esZj1ZLOlY5Kh4i/W+rVCq+Rk+8IeUtaHURH1HFCqyUDF8+NgqS8hBrd8GCHoCfneZdi9h7wHOcAEVh28CrJg6hgCKCQoYwc5HcLQL7+5c/2ZJEIEDTZg8mSrETH8kgvgmYezRKKQqHoR9+m4q2+0QqaSyQACKdfBGDoZB7yIA6OIgfJBUKWKFlsRAMVq+mgVKdTAgQ9InjdUpkCIK1td0f2hKjT20MbHDBozAgQZ1G4hCcq43D9nmt4eIWJJVEUTJNHvBCIn0VTWdMgSdeOPnqQyEF8eJp24UlNnADim4w+DiAqnml6Ja0/WJ6ilbrMOSJhv6SA+XWXP5VhVZmPxLYsNW7l5aJczeeMVgkQgEL5hx4In+hZOksAUb1EHl01EQBEw+pKdQLVxGntdVDbsvXfS5L+2+5Bvv7lz2h6JsRzsH4cYBDcOzwI6iGwOqJ3VReQq9DkjSBnL1f+egMUAMF2hFiskwHCZ3EJdtwwhgNuBOHZWIX18IrKXvDXOKk4kJMZad5mNqfCFzcCerLVtuVSzhN+Afot1HdggbEJCq1bYOc4Szb2rIOdgjTd1be+LVUUrZJy/LwA4l7Ee65ZB0FPiIOjTjS5pmkwtPJBahIpKjUXrezlgeTBwqqXlj0zSVxOypE5l7lEMajBKcGS6mV9p2gQO/t9KJq9KQpg4C4RH/FD5soT9mC3pMtSvnFAKJW0WFkKv/UOQwkYUTxU6BVTa8o+8uzBRNj8VlmsfwoAQQ6CC+5TKEi/ASBeKwgQwHuUAPGvs9yCn43ZJN6qGoecjTNsRMwTS2KTLAHiIVaqgySJMNgRbZji9EMEdxxzCaoXJ3Vkw/GUABnPFvQiKEyhT+F+09rugDABB7AACAYjsFlJAFG9Eol/aLJihGiEQUFWwEMoHxHFm9cNj1kD0X7tAEmoVgEQhFaRoAe1qdQnPHDOf7w0kIYxlDQrpeOwtEewS8r3Yjxm6T3eZLLM0DcPkISoUcZI8S7bpzk1pFiaFCGWTzIhYFhRV4965CGqfyBR9yQdRdoOOYgk8QEQdhtSDSwPggQ96NffKQB5G/Vb/fKXP9er0UhLbiDMk6SbgVD2EhxDg93SOCghf9BoII1UyIMsAxxRGada+1iMnzxjsaShSi5fkTH/H4PjwK7Jg4gJI0C8YIhEfYopi/OFjadzAgQNO/fb1vat+uYTQPD6UefAKDo4oCEmQGo2r3RCSHWy/BrEFpJqFAFrdA56VZ3v33MV3ORJPeIjdqDnVc/wPqJ2RfGGUE9eAgSIKuRyKZeNPqocZ+Eg8xIc0oM0VWWiDnr6MtwqbzhobIIz9HDk2BXz8nkjjPLwKryFQif3YM5iacqia95QHCTDdQ6QjXseeI7oPIwQi29WC9iVsnp+pUjie/Mg38lrPNQPEr/znWoi1S/9xL9AgPAWeZEsatoZILJYAIFmEetz5inuaUjeMhH0MKHoL+datmMoYs9zkLTmK00GzPx/TFaE1RFVfPI9HgIJEvXp1bVNF/AiAZCD3UPifpC0W4MC1M2IWUjDHj0ZOvxcDEMFBOY24X3lgcYSGyJX0fBujPIRuydP4sNRbFyPDONHg+YNgEgqAnBIzkJa17vpCAwfPZpurs/l8qv04IHnQW4dIIWW6dwz5JlZrudBMYv3Nv2cAyR0XyHHj54Mrbh2Na6PQorcJABS0r0c/0PQFB6EhULJ3ymD9z7133UA+XN/MACiPXuSdruHIAjE6ihLwSBkxfEMEwaoSisUk8cROGiVnPXlfQTte4qlOgEQz1F8LhMtoA8CDVSfYjYvft1w8TNAEPANmolNr5YCyfyKg++2LVarIdTSdEV6RST2BAheuwqFYKe4QB7eAMPNnLVTmKOuQ1XI0fNREyD4PK2A8BwC4ECIxV3hnofEcAI1R3mI5Ul6tnYaxJD+habNv3BZ7ONlLDyI3o+HWm9js9xjxMrss0TeDZ4oVh8Mx8WtvjXspKklkpxIA0bQhHSkSNI5gZFJutS9GNoQokYChCARixXVdDEHZa71vXkQXJfwEJ9UmPhJvQef+xd+7LM9Z67ignHpiS+/dGCo0KWYEQdrXJ0oPWG4RRo4cpVYg+Plw7ITF4OkCZAolcRuiRiUHFJ4Tcr9pACpanmQGb0IAHJ0gMCDFADxngwAZKTmX4ZJARCSE6j7YC1aJNFopIKcnUPgRjbGVERUykmBew7SY2rJgF4ky+nVA8JiYBp54/3wqZsOubbGJ30ngJQ3/TLESt1Phf6KhqYYq0NgQUvnuqfIPbSltjigkWMgSU9Ju2bvBkCUf5RJes416FFc1bt2vRZbc713HfcjAMJ9lcUwa+Vayke/2yT9nzpA/sMf/2xP+YA3NElyrmHQ5PUpp5AFhucYD06aFg4RI6YTAiQDBwrDXHWwaR2zixDT8Opz5a78jTdLkUnz8IDKXHwOelb7JWCRdmjk4pgfqHcHBoCM51jqqTCLNG97srttBgiPvC+bZyNU5FY8H7LimB07hKZq5KJEVE4gQkTHoOcf4UHkRSIPqah0ZhjGSSYqFKpRSot0JErUtiXlAYKg5q/okOZDrVpHovFisFr4dJ8wIlDlUTKhdI022pCupJwjhnS7IUwtCn6dw4soxIIHcfEiPYimsecEXd6GeYdTuOXAOXSO7k4+IQdTTg7w5mAX8Y6VpDPUhIHiTOdg6RQSB0BwTXLIV0j1i8axEkzZ0pw/R/n1EkyXucdbk/Rf+InP9dTwU+MfbIP+yISbbZWoYqAZxv6Mh51Nqp6jSCeDzhqAZAhlbMeanPsA7gkkbw+KF/lD3Jz0inVwU/NUYioFDsSz7FLETwyw0/xk693ODONyRrUd4KJZC5nb9OrGZssb5id4D7drAYQzaP3BQ8vtTHDumDQo+YyYOTzt0IYNcgUNk8bBh4wdk0hYefdwiwDBsGt6kgGrLEzmoUBwJqvBaKJm7GvcFKbgDwdAGKwiJ1CWmmYh88TgfSf9jS99LkIpbZxVch91DdZKXBUXdZVI4nnjfVg0jU4oQ51jJkkfVK7TvKxug6FCZyDFiS55J9OlSjuKimlNsyfaCq2HdsQ+FvSm73EfMOdA+jhQ9phhxjVyNuCZSPsw/f5Ths7Oxfw3S8o6DrJyvSB+5AkDAGppEPguD3545E8MkH//xz/Tq/HFe8Bd3gHLh7oA4/CBehFQy4YHGQ96G9OboF+ktxoDHYYsUzt16rp1rCBw10kGyi8k30iSfyv0Unuuc/y+/ShLKypeTPTLYwAA5O/QFaMWUs8WDpBrggF8+90Gey04QkTdiDifPrSMAGERVOouhIjIIRpMSBwDRAolCQh4B9RAHDTwpNrTGNV0BxC8bCMVtLwGcjkfmZoGzsmb+DIQxHMCiA8KZ6WcU5U0JpTn1z1wxKa8fhytmkgg+WsOzHM2LDZWpYKrmtZi8F25t9wzRmmtXPIOOlfsGQCiwQ00WJGDYF4vPY3rtmJuL6fXqDX6WDVMzHG/wnsIIJqQqXUIA2fD4lwEqSKaIhcm8yEvD3V0fAIkuC6co+DeJbpA8+DC89URl14ljMlDX6/+9I9BzSuAaMqeJu2pjbiSdaSlVxiFHGSMQ4XJiwVAZBR0ZxnW0NBlgEDgq/ExEq3FbCnac196w+HQpJZVk4kcFqGa5r9qagle4x4WZ1hbM8NKthvmIdB7QU2KJB1dhSgQyovkYXQuv0pJOVi5ybi2CSYkYv6XL7Kh12CYKcBIm6brAc/Bsfr4Gc69GrFQSTKAqgHvx4+cS0NJnEr39+YAiaU89ByqRqWHeteT7FnbvDj1MvrTXTAKKXuRO4UVTrMDNIMuTW1PIRkPbG5Vjr7wAAiKfAixGBWE2hdTIB0cMcE/pq/TE0HxUDX0PKpLKbwC/Y5mK4CkBEjM6aJq2FuQ3+ZBAiDRUBUgwd/FkGv8i2Y3/EypM7ucn1WGWx8LkJ/70g9osiIvhOyqUmfNsMWBwEEhdw9gWGtNhTzEbDwwawYIsVAf0UKYKA/L+pk8SBBZsYSFXWN5IywAoEKUL+T0mVNs2eVVU82CVs5HZQZA6snM5ssbTjmBl4HnWO9xQxQj2wAUro89desqnRm8BHRUA654IEAmGjzN/IPvXeCgYsD7NcTq6bWzp93l75C6sCLE0ERFNykIEKbkZTW6rlouo5/XwGqueebOPfxdr8C7ZIVhQRQGYah8jXHKYSSAk3Hx6ryiN/V0kGb39RMMqc4W5vgMLBT22DKLsU9HDmyAlg2JdhwgAiBNes9kinIoeRrFGSOXoUjFC5Ag3KI+C0+XchAfhucttwAJrm0JEHZQPtAElgf3DQkEbKIi0ZQmv8gfxHMFQOJnPjFA/tSXPttrDwRfuZ/yPBdKAIFdU0hS9zurcQkIDvWLjED9IpemeXbazYdTe5+UW6o8rl+qVj0rgMSp4pQj+J4NiA99+Fxa/MaCpirrCLE0J6umB1nePGYOggF4m/Zkm53UvBwWN2pEGCRdFfIGFPkwMhSTIzFTV0ttGFI5U6UFkU4jeIMRrTy/L/oW80JIatPVw0siNsc4UQGEBAhCFbeQKrz4wG0PXWjRT7nNFg1W+eFztJI0BeGs5C+6yTgFzg+xPhX/NFtAYQY8m0I8gIitsz6dRBP4FUoBFC1CqEPLjwCIvHpU7tVCHZQ8/74TCEkDhlOC4RqxgAhh71HTFkkHs0NUybrPrUgDrfE7H5eDPBRixTzl2NQsw5DzkQBIAKcEUckQvjVJ/9qXPtfvuQfCi4W+PlljM/3AUGSH2Phkw24L+8COPICECyLZVKXEUUVSv02uhpdlyZx/JF26QDqC0ZgTyOZ2KFZk2amRPAhrNRhLiVi3h2oUiz2Xtnz0hAPlABrosFZb9INs2EIMdomCTFMTlHpdKptNapti/8kCI3uG1jRgr6Ij0Nf2cJlNZpY4l9f7O9DGuz3sbHfAbnnsRgQ4tOySE134NXkUtgBI/MUwqT202nAVA77Dg8AgjVB3qa3BRyb+Aom8FzyI95m4dowLd3xTFd4fPg+ZitggaeHY1871FNoDs+cOQo0QDWm71q7p6wAMF9eENMIRkZYNFXIXhuSeY7HzM9a8YWgE99Pv6UlwzhRR5H2QCoeiyJytfiTqpQe5TNIvq+RBODCnK6r1ca6+a4B89Uuf65HgwIKzQOZdcGBvSG26RWUlvTvaqNu5wldhCqvS3sIa+qJsxfjW0vzXQLFiwzyaUlNONNkix5Ga0I7Qj/MHfZe6dzRpsLXL4NFVeP34iQ1HAMiA/SD36y0BokEBAkhJPICZw0ijGQEyt+l0ZOMxtmiJwiUF7HIMxrx+Q4mVYmrJ5rC3LVcUqKkIO0g0CNGbxcAQtuL50XgS423aowCiETwKT2J0ksSRME5eX/EeE/W/ayc7e0zYZyJtGEIzCSlV5Y9ecwDlcMS2Jk9+OUtX4dMeICENi8Q707dcz+b3KPath18KiT6vQ7FJNw5g2gHinoUaOvSpbwEQgZG5rt/zWLRDY+n3/zIHeVuIVdK8ZQiV5StvjmENgF3mJG/1IF/5Az/YR/LMnd+8MeqxRgFMhTHEhScCZNi1Yn84c1Z6IvAx8CiK04OviyKYMhq8gHhRcvslQHKVXQBRXQET2iFRoO31PgI8F6lb5uC11eOJXd08siUAAiCY2f2utTvsI1ytfeAcLJqYJ65B81rGpBnZFIPxrmYc0D2bostPTFLIzIHcmLJIq8g2Uu+V73vbno62J51YcUwOds2r/idJPzDQgsFhHqLCG4B09D0rDFGKWkh8zmSd0nBfv43XXoNZhHoY0xtRc/EckTIYF1G6+kFWWf0dhyMYI+0UZH84JyAqnMKoWSgQpLqVzipqUxKo6aRLM+Zr59xYSaYu9kpkQgy2yFor3C88pzyIaF+NQQ8ySLkugehH5uMAculBwlPEjLMADav+xdDrdI2LPOUThVh//PM/2HNQMvruQphHilOP8BIKGMlyEwhh0SOBhUpWs0dKgKhRJyxAeBCuHYjBxt6sQLbEXTl0TVglgAl9LZNtJWoAjdS/HRW9YI6m86Vd3dzY1fUNm71w4df7g728vbMXL19LAtHHhHG1xIp5Gtp0WtODLOZTepNJA++huBDJaLA9DKlGWAXnc6DA7vh+wC16HiCjH2KjLjbs4vc8nIMGrKuUtCP0YuHNe78xydxDIyW5PkPXi3T02GDOPeRjDYfJ+MG4BLSpbYKdJM2IY055v5yWF6fig6zZZYn1esb9k7s98gvknNoGBmPDlll+TSFQZF607j6dJRrFuFbNM50cKouqRQWeyTrmF/g/GrQuAAJQFgDxfIRVda/VXLJPZS0jcgvaRh8LW+YbnGqJ1ucRZp/1tt1uE3MaZ7D8+U8EkK98/nNcb8PiGGXb4v35cEk7vEPIGtJURYYaugrc9IpOPXiJsD/oIcFDfK9bF2WaslLOlLHUNvBkUkkaimzT2cyOHcKs3jZbjO85MaRw6Svl7vAe86slc5DpHLNxJxQeQm798vWtffjhCxaoWBMhmwUASD6C5Hwxn9hsPrHFbMrheGOsZKdESFVcMkB4TwhpOFrIpd0sQspc7tAkxEW5tSrkTjZo541mgzFxb2HRcjMSx7tG0a5gWyKGZqxMgOQZwVyB0B3k3cfIn3xQtuvBImeEd6fR86nx7XFAT7zd7my727E+wYVETpKQQkeo5e0CkTxLeBtLSSW6jA2w4Vpk7ZHkFx2JR+0aVICtmgZyHm6lAkCYC+VFolrLJkA/lKR/XA5SeoaYsI8RsHie9XpNdqtkwmIAXYSECchlnpXgbVb9yS/+cA/3jKoxK8dYSolknDVRjcihVQ3tTgxAjpyAIYnZsOsJEAkZxe+yzRM8L0CDK+XDpbJVgC2SbikWZMJCASCzGfagz7lS+vbu1va7LS0RNU+wElh1hor1eEpPgq9N2RcyNkxevFtv7KMXLzm8Ae23DMu43+LkAKntZrlgeHU1n3FCO9pnWeNgN5uW2XAUDRvJtEKOoQEYISTKo5G1NrRD7IX3qS0Is/Y7JMGttaBKDx0/Mj+JqnOa2K67ocub22KV93gjFrViyAmHAjJXc8N7YOVbbVNszWVY7Ik8nxCsoHcI9mhHrug9mJwfMJZH9SHUjiAmBA272e9tvcUuR/V9kOankqKmgWA7sdeAkkXmmjjQ2qCH4SVbO/kyTglXtVwV+U6EcQAmcsIYbk2KwcVY3wkgcXYjpIprhq9juiVmJAMoAPZut2N9BI+yJhLg/UQe5Gd/7Pf1aPoZN5BUSKtkGDOK0ZzoJiRDhesdojXN2Y198vIgAAi2qeZ8RCNCBBC+IFTifZpF5CAywuq8i10SsFDNeGyz6dyG9ZyroDfrNb3IZrPhQYEbncxmXKQDiQfXsMHzjLFcZ2LNbMYcBYnoar1jPrLe7W2HOLjd8yY3zciWV3NbLmZ2fX1FRmvcaHMUYn/ecK8OMw9AeOdxMw4XquU4MGt4OCbimBcGXVZjh/3Bdtu93a9Wtt3srCOThc1RMR9XvR7i70T7inDx4MXXklHK4uCAJ5k0tc3GNReDYluVdiLW7EdRxR+DKWSQkPzQqkN82KuvhjOSUyss1qiphx9FPKigt/vWNjhUrWohlN9QWoOwFLmbwpfYoyIaGSEwjABAdfTVcdoOwMOLcBUypnbv+Y/39DC0Axngay8oLXqT5g3rX3oRfB5SE3w/wkCAI4aIlxuqItQqB9B9Yg/y81/+/b3i7yFpXEwu6Q476pQoRsTcJwyHpjXqSKEifqUVoIuuoDDhBkLv3TOOcWNNBADx7jWM28FS+aEKOzEMgJM92CcRdQqEQI1NpzOrhlOzATRNPcHx0Ucf+VT1oV1dL7ntFoMmtOVIy3Pw38tHj+zq+pHNF1e23u7s5et7++DFS3t9e8fn4cplLAdi/jGzJ09vbD4b23TscnbuzPNhCfCIzEEaj9dlcekQByP76H5rL7HmbbUiuK6ulqR4293eXr58ZevVRufVqd9k8VLtQk1CKca/iPnLr4M0wcYveBDtRKxJpNDze3swy7I4uAfsO9SuF3RmongHLxtFUyy5We/3rBXBe6hnQ70bsPTIE+tGq+ToveCluKhUi4HwLxLhCGOYMKPnn31BauXVewcpANbO60FcrQf615uvfCZZVk6ca7EuwRE5CK4NEnGF5Q2XpMKLUDjqi1Tx2hBqwZvg88grPzFA/vN/8ws9QEB9F+a3cs0zxoxiWIOmBCAR52owdIi1iMN9My2nckiKgso6d6n7oAdOQ3cxVfR5kMXySikOigpDvkaGchaX1XsfdzNdMIwCmGAFbm/vWFWHRZ8u5tbAlWJHIawU5tkiXq9REZ/bDCrfORS+xiLVhy9e26vbO7tfbyRio8gSnrOx60dLepKrxUQrDEa1S75FRzOJtIq/e7/akrKENcZrWZ0qW4OpgpCyx4wsJOvYLXi0Fju+2QGIMyujoJ5+hJSwtsrFSK3zbzhj59cVNx7hTV5bhtBOqxzgZadTgLqhEiC0Yyzgjiqyc1gEitd1u9pxVhivZa/rAY8BT4F8hCybD07gLGUYB7zWpmZ4iQf2qzCs5dQbaM6UfNMqg7L2XYpcFxcLljws5x5G1nyc3ufMZdd5wXD4qNkykVZlPuv3VCfBWfRplZjAw2uNWEH6LsxHRh5K7+Ido5zve2jtQKauJVAQEpIEuai6MyQsxrjy+7/6x/5Aj+42yG77/mAdHgZRoCa8ayC0y51RCW0HdjpqmELs/cPNwbo2NlOBDkavA/5HgMk5REIfMWQuJqYODBXSUvPpgPs/EDYhtkU8v95sWdfADRpPpzYaYwILyH9ChBYPVnI4aAiSyXTBnSIYpvzhy9f2+m7F+ogmNoqiRJw9X8zt6mrOnATbaMeTsWo3xV4+WNWPXryyV69ubb3eOdEwsLae2AGDvyETB9vmQxBwg7nt1heAkg49dckCM2H3yeksAMKq+j50aIR1faD1wnZYTWTkkGjketzHDloaS0PHVAKQAh5VnMaPnS6Pbq64kXi7XtkHL27t9WrHXA1Fu1f3K/bOsKcc3oKVROWJbAfCAYIBwFoIDKGAEhpiTAwVrMeupfL9hnhfyDtOWEIK+l5ERErSvXuR7JGTBvgZtnnzvz3v4hnRIp2sFTuf24XnjBA7drFot4rIgNo9K8kchMl+n2PwRHiTA2QpTghcSuZD1ZDylF/7419UVQGjfQgQzEnHYDjK2LRgEjOTGGNCNgFOHSxEBkgDFwzlK6q8SOz5HB4LBzjyEEG+oYcBEnI9fazHuCG1x7kqwsXebQxpqCcNFbgAx+HYWsPEdWLjyczqZsqwCJqs9fZgL1+vmLBT8OhF/d1+x/gZAMNeQYRb18slXbXmx2oq+2a7t7vV2u7v1rbagDrUPCjWFeqxHbHPHRaUXsGHzILkiBUG/vdwL3FjAWJptLTRNoYm0KsgTEweJKbCi8hQ/UCTCdnERboXIdeIXoS1nPEw7XQ5Hna2ur+1V6vW7neYhjlgOHV7v6YCgTmVLzkSA6G+ffrLGM+EEBh5EICB+cZ1wzCNA/eYhEMmo0FzsOYaVyrLrygBIPA5v5TzwzP4hjKvnTAshPrZBYa5R8TpYwdNhFaxchuhFPc5OqMYX5cYEgZFw++oRva6CEJs1kc8d7msxIsezyWH6r/5yufx6wRGPCQMkftXF5mSMG4MQniFSYyoLZhuoACCEAsFQ4pA3IIoBynl2Y6ORKTljRnRTJR32GGtAUIyGR9JNxiSIQTDJtkJ6OApYGztYUfLj12Bi/nShvWEIMYIoDvsClltbYfNLi6Ug4VB4r9vd9YelLhjx2DEsQhh4K7xWG+Qx9zZbtfyOahjwnhNGA2sK0ZDFzbT+upmhBjq3/cdh0llU5FhgaVDMIVrSnau7H1wyQV3GELiw7DTHzyYkTgrKVfffGWL2cSur6b0HtMGxV6EfVhiemuvNkdb7dUfA4DcwQMyiUKdRgwaOQJqwlTnwn3RFBJ4aOnZ4Emw/i7UuC7ZFl3NfCNCInkQHXRNTCFlTmk/8jfVmiJ2YGjDNgZ5kNQjkgY66HkjOQcwaGggOcJ1925YfA0PhN34x8F3vhkLvw+DhBArABIhXTqMiU4Uo0hD/le/+q/3ePFaNQCQ6A1Edx8A0qIaC+oPltNnVcmDaIJgg22vw8rG7CxES6teNP8e+6LU433+TzWRcqVMdNvpdmmoAmHqXXiUjfCGDRkPN9OJLZYLghkAmSDkmM/tevnIBsOGSeft/dZe324oXkQyrxhawANAtjs9lGD6fKzRyKazuS2urm25vCZAXr2+1/aqQ8fnQsgHVgxT6yGe4OJT9sz47CwOfRBA8r+KBVDy9NwDfiJDF9dKM3BFCeMaqDio68yuSB5UHwKB4X4UdmpKy/Jqak9ultxpP4VArmttt13benVvH60OdrfTti6shkAOwo1fkF/zNkmqwrj/0LrkRcs3ec251QscOPZDItHXfQjJHdknP/RiO+VFohMR3iU8SZTLRWP7xi3OVZaAstRSlZ6krKADGLiGuD7w3HgLYPtgfLBElYpgzDAAVR8qYwcbKF/mWEUhuEzceTWybNCq//ZP/is9UKZeY2/ej6ELXGemEZQEiAvNWGRybRP6rpF/jFFLGWDI3MmGHDInkHF6uu/zS62gZ0gJOxIV2pyTUBPEhE+CRbp2t28DhBfTiV0t4S1wI7vkQZCg9/2ADM09ad49vR9IaIRt4PRZwzjsmbgd8BHaJLA5TDZ71mFuHj2xJ0+f2XZ3sNevV6QksaDy/n7DRB11hUO3t2MnxgjvTzJ57zZ0VbBiZL0vLfpUHI/rCvJ', 1),
('USU-002', 'CTORRESA', '123546', 'CIELO TORRES', 'cielo.torres.araujo@gmail.com', '2018-01-02 00:00:00', NULL, 1),
('USU-003', 'DTORRESC', '123', 'DEYSI TORRES', '@', '2018-01-01 00:00:00', NULL, 1),
('USU-004', 'YTORRESC', '123', 'YUBARLENI TORRES', '@', '2018-01-01 00:00:00', NULL, 1),
('USU-005', 'XTORRESC', '456', 'AXEL TORRES', '@', '2018-01-01 00:00:00', NULL, 1),
('USU-006', 'ETORRES', '456', 'EMILIANO TORRES', '@', '2018-01-01 00:00:00', NULL, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbinvmaarticulo`
--
ALTER TABLE `tbinvmaarticulo`
  ADD PRIMARY KEY (`art_codigo`);

--
-- Indices de la tabla `tbinvmaealmacen`
--
ALTER TABLE `tbinvmaealmacen`
  ADD PRIMARY KEY (`alm_codigo`);

--
-- Indices de la tabla `tbinvmaecaracteristicas`
--
ALTER TABLE `tbinvmaecaracteristicas`
  ADD PRIMARY KEY (`car_codigo`);

--
-- Indices de la tabla `tbinvmaecatalogo`
--
ALTER TABLE `tbinvmaecatalogo`
  ADD PRIMARY KEY (`cat_codigo`);

--
-- Indices de la tabla `tbinvmaecataxcara`
--
ALTER TABLE `tbinvmaecataxcara`
  ADD PRIMARY KEY (`cxc_catcod`,`cxc_carcod`,`cxc_nrosec`);

--
-- Indices de la tabla `tbinvmaelinea`
--
ALTER TABLE `tbinvmaelinea`
  ADD PRIMARY KEY (`lin_codigo`);

--
-- Indices de la tabla `tbinvmaetransacciones`
--
ALTER TABLE `tbinvmaetransacciones`
  ADD PRIMARY KEY (`tra_codigo`);

--
-- Indices de la tabla `tbinvmaeunidades`
--
ALTER TABLE `tbinvmaeunidades`
  ADD PRIMARY KEY (`und_codigo`);

--
-- Indices de la tabla `tbinvmovguiacabecera`
--
ALTER TABLE `tbinvmovguiacabecera`
  ADD PRIMARY KEY (`gic_correl`,`gic_fecreg`,`gic_tipo`,`gic_tracod`);

--
-- Indices de la tabla `tbinvmovguiadetalle`
--
ALTER TABLE `tbinvmovguiadetalle`
  ADD PRIMARY KEY (`gid_correl`,`gid_fecreg`,`gid_tipo`,`gid_tracod`,`gid_artcod`);

--
-- Indices de la tabla `tbinvmvespecificaciones`
--
ALTER TABLE `tbinvmvespecificaciones`
  ADD PRIMARY KEY (`vcc_artcod`,`vcc_catcod`,`vcc_carcod`,`vcc_nrosec`);

--
-- Indices de la tabla `tbusuarios`
--
ALTER TABLE `tbusuarios`
  ADD PRIMARY KEY (`usu_codigo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
