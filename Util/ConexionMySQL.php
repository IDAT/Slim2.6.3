<?php

	class ConexionMySQL {

		public static $mysqlHost = "127.0.0.1";
		public static $mysqlPort = "3306";
		public static $mysqlData = "dbinventarios";
		public static $mysqlUser = "root";
		public static $mysqlPwd = "";

		public function __construct(){
		}

		final public function getConexion(){
			try{
				$dsn = "mysql:host=".self::$mysqlHost.";port=".self::$mysqlPort.";dbname=".self::$mysqlData.";charset=utf8mb4";

				$options = [
				  PDO::ATTR_EMULATE_PREPARES   => false, //Desactivar el modo de emulación para prepared statements
				  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //Activar errores en forma de excepciones
				  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //Recuperar por defecto en una matriz asociativa
				];

	            $conn = new PDO($dsn, self::$mysqlUser, self::$mysqlPwd, $options);
	            //$conn = new PDO($dsn, self::$mysqlUser, self::$mysqlPwd, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
	            //$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	        }catch(PDOException $e){
	        	throw new Exception($e->getMessage());
	        }
	        return $conn;
		}
	}
?>