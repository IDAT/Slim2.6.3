<?php

	include_once 'Util/ConexionMySQL.php';
	include 'Dao/AlmacenDao.php';

	class AlmacenDaoImpl extends ConexionMySQL implements AlmacenDao{

		public function SelAlmacenAll(){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcAlmacenSelect()");
			    $callableStatement->execute();
			    $resultSet = $callableStatement->fetchAll(PDO::FETCH_OBJ);

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;
		}
	}
?>