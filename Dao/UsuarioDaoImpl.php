<?php

	include_once 'Util/ConexionMySQL.php';
	include 'Dao/UsuarioDao.php';

	class UsuarioDaoImpl extends ConexionMySQL implements UsuarioDao{

		public function SelUsuarioAll(){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcUsuarioSelect()");
			    $callableStatement->execute();
			    $resultSet = $callableStatement->fetchAll(PDO::FETCH_OBJ);

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;
		}

		public function SelUsuario($codigo){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcUsuarioSelectByCodigo(?)");
			    $callableStatement->bindParam(1, $codigo, PDO::PARAM_STR);
			    $callableStatement->execute();
			    $resultSet = $callableStatement->fetchAll(PDO::FETCH_OBJ);

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;	
		}

		public function InsUsuario($body){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcUsuarioInsert(?,?,?,?,?)");
			    $callableStatement->bindParam(1, $body->usu_nombre, PDO::PARAM_STR);
			    $callableStatement->bindParam(2, $body->usu_passwd, PDO::PARAM_STR);
			    $callableStatement->bindParam(3, $body->usu_descri, PDO::PARAM_STR);
			    $callableStatement->bindParam(4, $body->usu_email, PDO::PARAM_STR);
			    $callableStatement->bindParam(5, $body->usu_estcod, PDO::PARAM_STR);
			    $callableStatement->execute();
			    $resultSet = $callableStatement->rowCount();

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;	
		}

		public function UpdUsuario($body){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcUsuarioUpdate(?,?,?,?,?,?)");
			    $callableStatement->bindParam(1, $body->usu_nombre, PDO::PARAM_STR);
			    $callableStatement->bindParam(2, $body->usu_passwd, PDO::PARAM_STR);
			    $callableStatement->bindParam(3, $body->usu_descri, PDO::PARAM_STR);
			    $callableStatement->bindParam(4, $body->usu_email, PDO::PARAM_STR);
			    $callableStatement->bindParam(5, $body->usu_estcod, PDO::PARAM_STR);
			    $callableStatement->bindParam(6, $body->usu_codigo, PDO::PARAM_STR);
			    $callableStatement->execute();
			    $resultSet = $callableStatement->rowCount();

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;	
		}

		public function DelUsuario($codigo){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcUsuarioDelete(?)");
			    $callableStatement->bindParam(1, $codigo, PDO::PARAM_STR);
			    $callableStatement->execute();
			    $resultSet = $callableStatement->rowCount();

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;	
		}

		public function LogUsuario($body){
			try {
	            $connection  = parent::getConexion();
			    $callableStatement = $connection->prepare("CALL prcUsuarioLogin(?,?,@cod,@des,@ema,@est,@exi,@val)");
			    $callableStatement->bindParam(1, $body->usu_nombre, PDO::PARAM_STR);
			    $callableStatement->bindParam(2, $body->usu_passwd, PDO::PARAM_STR);
			    $callableStatement->execute();
			    $callableStatement->closeCursor();

			    // execute the second query to get customer's level
			    $SQL = "SELECT @cod as cod, @des as des, @ema as ema, @est as est, @exi as exi, @val as val";
		        $row = $connection->query($SQL)->fetch(PDO::FETCH_ASSOC);
		        if ($row) {
		            $resultSet = $row;
		        }

		    } catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
			return $resultSet;	
		}

	}
?>