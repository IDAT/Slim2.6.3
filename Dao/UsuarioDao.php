<?php

	interface UsuarioDao{
		public function SelUsuarioAll();
		public function SelUsuario($codigo);
		public function InsUsuario($body);
		public function UpdUsuario($body);
		public function DelUsuario($codigo);
		public function LogUsuario($body);
	}
?>