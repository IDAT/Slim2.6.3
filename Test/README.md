# Testing


## Metodo GET

### Request:

`http://localhost/Slim2.6.3/Usuario`

### Response:
    
    {   "message": "2 REGISTRO(S) ENCONTRADO(S)",
    	"status": true,
    	"data": [
        	{
	            "usu_codigo": "001",
	            "usu_nombre": "ATORRES",
	            "usu_descri": "ALEX FERNANDO TORRES CHAHUARA",
	            "usu_passwd": "123",
	            "usu_email": "d15105@idat.edu.pe",
	            "usu_imagen": null,
	            "usu_fecreg": "2018-03-22 00:00:00",
	            "usu_estcod": 1,
	            "usu_estdes": "ACTIVO"
        	},
	        {
	            "usu_codigo": "002",
	            "usu_nombre": "CTORRES",
	            "usu_descri": "CIELO TORRES ARAUJO",
	            "usu_passwd": "123",
	            "usu_email": "d15106@idat.edu.pe",
	            "usu_imagen": "2018-02-01 00:00:00",
	            "usu_fecreg": null,
	            "usu_estcod": 1,
	            "usu_estdes": "ACTIVO"
	        }
    	]
	}
    

### Metodo POST

### Request:

`http://localhost/Slim2.6.3/Usuario`

    
	{
        "usu_codigo": "USU-999",
        "usu_nombre": "ATORRES",
        "usu_descri": "ALEX FERNANDO TORRES CHAHUARA",
        "usu_passwd": "123",
        "usu_email": "d15105@idat.edu.pe",
        "usu_imagen": null,
        "usu_fecreg": "2018-03-22 00:00:00",
        "usu_estcod": 1,
        "usu_estdes": null
	}     

### Response:

	{
        "message": "1 REGISTRO INGRESADO",
    	"status": true
	} 

### Metodo PUT

### Request:

`http://localhost/Slim2.6.3/Usuario`

    
	{
        "usu_codigo": "USU-999",
        "usu_nombre": "ATORRES",
        "usu_descri": "FERNANDO TORRES CHAHUARA",
        "usu_passwd": "123",
        "usu_email": "d15105@idat.edu.pe",
        "usu_imagen": null,
        "usu_fecreg": "2018-03-22 00:00:00",
        "usu_estcod": 1,
        "usu_estdes": null
	}     

### Response:

	{
        "message": "1 REGISTRO ACTUALIZADO",
    	"status": true
	} 

### Metodo DELETE

### Request:

`http://localhost/Slim2.6.3/Usuario/USU-999`

### Response:

	{
        "message": "1 REGISTRO ELIMINADO",
    	"status": true
	} 