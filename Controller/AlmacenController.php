<?php

    include 'Dao/AlmacenDaoImpl.php';
    include_once 'Util/Constantes.php';

    class AlmacenController {
        protected $app;
        protected $response;
        protected $request;
        protected $dao;
        
        public function __construct(){
            $this->app = \Slim\Slim::getInstance();
            $this->response = \Slim\Slim::getInstance()->response();
            $this->request = \Slim\Slim::getInstance()->request();
            $this->dao = new AlmacenDaoImpl();
        }

        function SelAlmacenAll(){
        	$this->response->header("Content-type", "application/json");
    	    try{
    		    $resulSet = $this->dao->SelAlmacenAll();
                echo '{"status":true, "message": "' . str_replace("{0}", count($resulSet), AVISO_CRUD_READ) .'", "data": '. json_encode($resulSet) .'}';
    		   
    		}catch(Exception $e){
    			echo '{"status": false, "message":"'. $e->getMessage() .'"}';
    		}
    	}
    }

?>