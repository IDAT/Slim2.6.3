<?php

	function verifyApiKey(\Slim\Route $route){
		$app = \Slim\Slim::getInstance();
        try {
        	// Getting request headers
        	$headers = apache_request_headers();

	        // Get Handshake KEY
	        if(!isset($headers['Authorization'])){
	            // api key is missing in header
	            $response = array('status'=>false,'message'=>"No Authorization");
	        	echo json_encode($response);
	            $app->stop();
	        }

	        $auth_key = $headers['Authorization'];

	        if($auth_key != 'P@$$w0rd801521'){
				$response = array('status'=>false,'message'=>"Error Authorization");
	        	echo json_encode($response);
	            $app->stop();
	        }

		} catch (Exception $e) {
			$app->stop();
		}
	}

?>