<?php

    include 'Dao/UsuarioDaoImpl.php';
    include_once 'Util/Constantes.php';

    class UsuarioController {
        protected $app;
        protected $response;
        protected $request;
        protected $dao;
        
        public function __construct(){
            $this->app = \Slim\Slim::getInstance();
            $this->response = \Slim\Slim::getInstance()->response();
            $this->request = \Slim\Slim::getInstance()->request();
            $this->dao = new UsuarioDaoImpl();
        }

        function SelUsuarioAll(){
        	$this->response->header("Content-type", "application/json");
    	    try{
    		    $resulSet = $this->dao->SelUsuarioAll();
                echo '{"status":true, "message": "' . str_replace("{0}", count($resulSet), AVISO_CRUD_READ) .'", "data": '. json_encode($resulSet) .'}';
    		   
    		}catch(Exception $e){
    			echo '{"status": false, "message":"'. $e->getMessage() .'"}';
    		}
    	}

        function SelUsuario($codigo){
            $this->response->header("Content-type", "application/json");
            try{
                $resulSet = $this->dao->SelUsuario($codigo);
                echo '{"status":true, "message": "' . str_replace("{0}", count($resulSet), AVISO_CRUD_READ) .'", "data": '. json_encode($resulSet) .'}';
               
            }catch(Exception $e){
                echo '{"status": false, "message":"'. $e->getMessage() .'"}';
            }     
        }
        
        function InsUsuario(){
            $this->response->header("Content-type", "application/json");
            $body = json_decode($this->request->getBody());
            try{
                $resulSet = $this->dao->InsUsuario($body);
                echo '{"status":true, "message": "' . str_replace("{0}", $resulSet, AVISO_CRUD_INSERT) .'"}';
               
            }catch(Exception $e){
                echo '{"status": false, "message":"'. $e->getMessage() .'"}';
            }
        }

        function UpdUsuario(){
            $this->response->header("Content-type", "application/json");
            $body = json_decode($this->request->getBody());
            try{
                $resulSet = $this->dao->UpdUsuario($body);
                echo '{"status":true, "message": "' . str_replace("{0}", $resulSet, AVISO_CRUD_UPDATE) .'"}';
               
            }catch(Exception $e){
                echo '{"status": false, "message":"'. $e->getMessage() .'"}';
            }
        }

        function DelUsuario($codigo){
            $this->response->header("Content-type", "application/json");
            try{
                $resulSet = $this->dao->DelUsuario($codigo);
                echo '{"status":true, "message": "' . str_replace("{0}", $resulSet, AVISO_CRUD_DELETE) .'"}';
               
            }catch(Exception $e){
                echo '{"status": false, "message":"'. $e->getMessage() .'"}';
            }
        }
    }
?>