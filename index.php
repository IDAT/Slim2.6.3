<?php
// allow api to get request from any domain
header ("Access-Control-Allow-Origin: *");

// TimeStamp Zone
date_default_timezone_set ('America/Lima');

	require 'vendor/autoload.php';
    require 'Controller/AuthentController.php';
	require 'Controller/UsuarioController.php';
	require 'Controller/AlmacenController.php';

	$app = new \Slim\Slim();

// GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8"/>
            <title>Slim Framework for PHP 5</title>
            <style>
                html,body,div,span,object,iframe,
                h1,h2,h3,h4,h5,h6,p,blockquote,pre,
                abbr,address,cite,code,
                del,dfn,em,img,ins,kbd,q,samp,
                small,strong,sub,sup,var,
                b,i,
                dl,dt,dd,ol,ul,li,
                fieldset,form,label,legend,
                table,caption,tbody,tfoot,thead,tr,th,td,
                article,aside,canvas,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section,summary,
                time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent;}
                body{line-height:1;}
                article,aside,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section{display:block;}
                nav ul{list-style:none;}
                blockquote,q{quotes:none;}
                blockquote:before,blockquote:after,
                q:before,q:after{content:'';content:none;}
                a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent;}
                ins{background-color:#ff9;color:#000;text-decoration:none;}
                mark{background-color:#ff9;color:#000;font-style:italic;font-weight:bold;}
                del{text-decoration:line-through;}
                abbr[title],dfn[title]{border-bottom:1px dotted;cursor:help;}
                table{border-collapse:collapse;border-spacing:0;}
                hr{display:block;height:1px;border:0;border-top:1px solid #cccccc;margin:1em 0;padding:0;}
                input,select{vertical-align:middle;}
                html{ background: #EDEDED; height: 100%; }
                body{background:#FFF;margin:0 auto;min-height:100%;padding:0 30px;width:440px;color:#666;font:14px/23px Arial,Verdana,sans-serif;}
                h1,h2,h3,p,ul,ol,form,section{margin:0 0 20px 0;}
                h1{color:#333;font-size:20px;}
                h2,h3{color:#333;font-size:14px;}
                h3{margin:0;font-size:12px;font-weight:bold;}
                ul,ol{list-style-position:inside;color:#999;}
                ul{list-style-type:square;}
                code,kbd{background:#EEE;border:1px solid #DDD;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:0 4px;color:#666;font-size:12px;}
                pre{background:#EEE;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:5px 10px;color:#666;font-size:12px;}
                pre code{background:transparent;border:none;padding:0;}
                a{color:#70a23e;}
                header{padding: 30px 0;text-align:center;}
            </style>
        </head>
        <body>
            <header>
                <a href="http://www.slimframework.com"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAMAAABrrFhUAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAMAUExURf////r8+IeuWXOhPpu6e4GqUfz9+3CfOW+cQf7+/nWiP+Xt3Nznzm+eOKzIjXCfOHynS4OqXLHKkm2dNa/JkbjPnfj69eTt23GfOnehS4CpT3GgOoOqW/7//oesYHahSnOhPf3+/fv8+vX48sjZtvT48ZK0cPn793GdRPb483WiQPj79v7+/W+eN/n7+HCdQtzn0Pf58/3+/PH27X2mU3iiTabDhYuwZ7XMnnKgO3ikRMvcu3OfRt/p1ODq1f39/H6oTIWrXnajQX+pTvP38PT38fP27+/06numSPv8+bLKms/ewKvFkJK2aXmlRujv34GqUHekQ2+cQoiuYvL27e706d3n0ZGzbm+fONfkyXSiP3KeRezy5vr8+Zy7fevx5Pr7+IqwXcfZsuLr2I+0ZbzRp6nFiObu3PH17Pz9/O7z53CfOtrmzIyyYMHVqtzn0HGdQ////nSgR+Tt26HAfoivW/D16nqmR9Piw3WgSejw4YGoWIarX3qkT4KpWvz9+rDJl9bjxqK/hZm7coOsVPf59OPs2YSqXbTLnObu3q7HlKjDjMPWsJG1Z87evMnatKvHjNjlyurx47rQpH+nVqfCipO1cazGko6zY4SsVXShPsLWq5a3dbTNmLzSo6TCgs3dur/UqHmjTvP379Hgw87dvqbBidThxsrauI2xaZe5cJW4bZ++eouxXtHgwK7IkMTXraPBgOHr17rRoKC+gqTAhpq5eenw4tjkzPb59LTMl6C/fHynScrbtt3o0LvRoqzHjZS3bIKrUoWtVqXDg6vGir3TpdXixczcuL3SpHukUdbjyZi5eNvm0KnEjpK0b7LLlI2yYZ29eNLhwt7o046ya568f3agSsXXs4quZJW2dJy8dszdud7p0o6xarDKksfZtb3SqJu8db/Tq+Ps2sHUrZW2c5CybLfPnL7TpsbYsIasYNDfvnukUJ+9gbXOmrbOm5e3domwXK/IlrHJmIWrXbfOoIGpWZC0ZbjPoYquZbXNmoqvZYSqXJ68gIuvZixoldwAABMUSURBVHja7J1pTBvpGccHaEwd45ADjBvHWxtK3XrtSjGVq0EbDHgRpi220ZKKWGQDhjoH2BKSOTbJhhIgqN0aAt1dcTQc9YdWXOGQFhHuG1pVJdAvq2walVZtDqSNqFalG1Wt8bzjCx9jYmN75/1/gpl3xvP8/J7P+zxjBIGCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoIipQSMzDFaXErKCO36eYaAXLZzKx7rFh8VynL4KhTl8HNkA4/EM6UVXFIYT60febqbjjIjHcVK3529YBR5vgNDvbS0pGaEpvlXaU/mWZGuJdVvZ3poDoJ7HFNBzoNQNF9yQT8Z6UEcpeYOxd1NwnbM5eqoodfy4/QRkUTEL54adwMg3FzoSMgBML6Ul+w3NjraGQJ0QJ34VQMwUsyxNZF+iq+d6BJr7s+KFye0/FN0x5ZQuEL5KgEQDEptrKt8ONx0OYNCZZvPsamUxEuDS61y+5EhZ7jFawDckZje2rUKdtDZT1m12o+2nUzJclZG0dFXaVsRWIYWLwFIepL3TmlLg612cKeUFvO1q8YyV+Xix54N2AySaKyXAFbTzaeYOylBVv+nUnGTdpoK3JcNqxmutBBY9w6AsQ6/cE4STPazN/H6336/gsBkoWi4EusNpArvADRbOhFUEUwA7szj07yhLGI9xnSfcM+KNS87wU0hDkB4IYjsrzdgD1USHkW4d65eDWdy9Ge9BFBl6UKZQdQJlHWC8X8hypvLMmd6jN7OA2g5OIBCXvAAuAY6AH63j0Zn1wAkTwDr04NBNAI+wea/7a9EiL8BIAVLqr1Tbaui4AHQAkbAuXHE/wCQi91bOwOaoiCyvywWTO2bkcMAYBpBeLygciy9CeYm/ZRDAhBseox1S3wfjsuhBWAdDEtnSQpAMuBmUk8GAFewhY0qiqwASlHzw04mkhVAEzY7nxeRFYAOA1BIJSuAZQxAK5usADrJDuAZBkBL2iagw3w0bVyyAmjKMz+sMp6sADaxiaD8GlkB0PjYYriRrAAYwB2yKCApAGovBkCZSdbl8BoGIKKJrAAURzECDxNICiC+Fez06kgKABkCu72yIpICSAoHG2N9FeQEULYMqgBrq56UAJCz5fiG3YaRlACQTT6+ZVubT0oAFI0l6Gd3uoyEAJDxWkvkRnLs2yQEgGQaLBGSJdnNFPIBQK4VW+KemHJ9ETu4AFCrs8LC4omGVVHiTYUp3jbl3I12a/RbhP5cVnAA4CYW6TQ7k3wOPbq90jDbc87oznUVNtb4oFea3p4XqcpZmHgw2MLwAgNjNNlKoCR5ozHBLwDYiqGOjlXnwy2ju0PXbPs9J2wv1h21C9FVHRGXugiuEV0eNeQIbQKbo1F58bMx4r4+ymC4bag0Wv6qhet7ADQlXShk9Tvbiq0eLhEKk6271ImjCywngdrp2m0n1ZMS1Z+e5yS0v7I/hfieT65Gbne16tFUhcTHAJ5gm/HTTk5d34sipfeA/67MSF3lbahqpx2Mymru5bgM7e+6RLwS1BgcEmWU+s9yqb4EcAzrZJztxdLMC/NX5mZL2a5juslXON4RZnvlnYl0N4XRtkHilYAxVYg6XH5Ev1nPPjwAW3v+aWNXuvuMDdmo1Y0d1iP3kN8hG/JiZOdNPXTMGhFKu5oZhwWgztT5jmltujJ+3UNDdm9fdnG5zKaRy2PxJ7rUa9tRpBYWG7Jrsw2tSo5t6R5v5jYJq9novs7EMHqdeigAlEakyGI/S2Z4WppbzaWaJGEo1P2TljFBNlNgvupcoeUhj5avN47xru4VFlAqLnxebu0XZN7FQPJKNZWOCJjH9Su8wwCQG1cImn/Ewmj+RbsikoqZVItNOpNN1Eb8QVHlsahx+09kNNdavsnyOG8nx53ZRx2HFWarOpPqbwA5OuCgQRe6nbU7YwweblsZhVDVYCGL1r1ytpAVVA2AoR3t9Xpew4jS7OuJ25UxCqp/AUSCwe/45y4c9YJu/EvvS+pWgYFx8bKLfY3ztaDRoAfweQoyaiakjgxkS2MCvwLACmhTXM7C2M2AACsbdP/zateNM6EW3DL8YB6/K+o+x+5APuuhFrw+gAhxrrsPWAGDJNb9q3ZT3PVwxjlQe5cPuDahKHStDqOyUlfvVwDoffdRi+xOm0miquu8ewPiwDdYeHCnb1jKktRuZOQ8XBH5DwBn1tNatD7bar/YoyMzFusGON2vsy7NeL6bY5czeOyKvwBE3PU83Fbhk7WjYs+9e8IRrKz+9ULheNN281PWXJF/ADCHCQxYBaAKcIaTCDz6DFYFUpOQ15OINmzjM2DJpq76A4CKkGdajYX2VF4m5O/CMuI5Va/vm6IN26TP5qyF+QHAcULT9gSsNspaCPmVNrAH7vSFC316i28hkDx6NVAAEKwNRGwTKvwcGzDXfRIMeLHJ+oaJ9M74QAFYxtoAsddU0LA+M9tHqWpj/ZbeMEcnChCAKAyAnlDpeGwckOX6yE3N6LR0hslVAQKQKcNyHYklevRhFXbaV456UZNlftyWERgAPMwPICc2vYsxr2zpy74CYFqH43UAfSAJCAAJ+FKJBXc1mRtMXiziOwJNOIHjjwMCoGwj2gsAK950mQRbwTLucJqIDwQARGyu1enEQlynMQAnfQgAefMRPhbGBQTArBmAqpRQ4ZFTvgeAKPBGcC8gAGLME3zmM0KF4/wBAE85jGw7GzgA0bEBBICnXbNWSAqAi4fWdJIUAFID3BITIpICMKY67QTIA0AAHK5Se1dLAWkAIE/BjqT9dKxeSRoAq8DZZj8dqz9NGgDbQpID2IQAYBMgN4AOzNuaWkFWAMNgHmAkKQBendM3o5EHwGPgHp9lkxOA4C7Yzq1ByAngGlgLSc+GNoCkmoPlSrHBixiZMUhIA4ib49Qe6A2nY+BFjPu2qEMLQKaBFYnOV3mfPc/YAtGDdyWhDICxZd7qlT8P89L+6pdgmzx1X4xCSAFQg63u5MUr3gHQAfs5L0UhDWAUD/hghld5EVEtGsL3hVr3v4kzpABc11pCXnI054nan7WMh4konbwfOLQ6QdqANVVqZ5VYCEX9Sdz+09vUUAeA5PdaYyDR4k3PCESPi/ErpN3OInpDbSJU/9QmBJIz1+ghOSTpniVDRdnkNKI55KbClMY2m5jw9t0hN1EXFT0DljwM6ZTzZw3BtUBml10crFyzkunkMaj1UbM2+UmnN13MnkJxMUQZLLTL0qNP1g6dy+fZmJB1qebJjk1F4bS6jDgKzdVgxrMj9omKLL68fGL23kxHR8faq7td2ZMRtoml/HXXgbehuhyuOCnfn6kanUen0/Mcs4ciit39SkrI+gPY1++VowR+Z4qjfe42oDuEHSLUXLWW7sF8YeFzDwGXBwSgwGZX4QTTldexEJkhQoVppwi/PpqreKB1nbeKSvurLnq6BWU32vx7Lc7O1ewNN9GTzqLcx83vtOIQjWiuMvNSEnNljJt//01K8AXqvJHOjflI+r5s9JI6/WfXiDgOFHfFYnGP03pS3agxnatyOoFSHDOdWssguB6hNJpKL0URzIS6c18s1qxUE17tSYxF6qXaBU6kUCg8JRTSI2WGpUZaRhD9LsKhiMtLGlPcGRn5eX5iPAIFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBeVnZdD+/JO4XB/e8Ecn7PUL07F//Q/XDz7+5IO//pFhH43Je/cNoH9aozoTPm5o+CLNpJsvXnwAjv3uxd6BLxoaGk58+V3T/2Gf2n/WPy5SGm7cOnOiIS3tl+YLRB+9MOlmWtqvzeGy195oOHHm1r9v/BTcT5D/n7+8/95bb733my9/RrPGmt5Ou/nGO+98uve43/7ko1/9wfK4KeBz/rb3D/XdF2n/vXXjww+//1s7a771dXvdNh37msOx3//QLp/u79+xnLHeK/GMtfwtwOW29dA39sLLv/dN+/ueKeD+Cfz5Y+zrfR/8+//2zuWlrSwM4GdAwg1ducnQhSmJUlOQRopSIbGD0AcaoZCkdRGNQilkurJZDFjHWoSCr5XxsXFlTVu3Ymz8F6rRjaO2C7HU2oVUEPugwzCZe8/3ndfNjTHuBu63Oufc13d+9zvf48iNA/RHSW5gbxVu96i9T1zryzxhj75coC7+rlM9DjQZnVcx7HV4ygagaS370iXDYnzKEkAvnF3XXxIAOcHmAb0ix++QpRYEnSRY/Oo39erJdBEAmna4XwDAO4udS83kHAC0b8LYnU/F8BevFQCN2hy5p5UGsMpmY0KbMrpt0J42PiCoCm2bdQoGqooA0CIvzACyQezMOM8FADSistkrRv2rlgDi9N9lBc4AoLMRj9MPcBf4oQpDzSVoPzMONTcW6tTYXAyA9tAEwHGM7R9mB3oKAH/bs6FJNt7Bf0YdrPYiDK9bApijH32snwFAK845eUE/HhXHD3Ugzjxthg3IPbsWs9Q6aiUAwZnhsUp2ZNqrAqhnfquGFAHQtAkyIQD4dMNLdODxSvaRjAuW4hCYVOVzFcCf8CBjDTojRitvAeA6PmtfUPLP6+15vzBvXY9rcHqD4YHHOdv1vddjF5kBBiQAjfojXkwzp3VFAVC3pknLyRJAQBoTAIRS/ezr+ix403GYmS+rAtgC3T7oI91zeqPvyALAe+lZ6TCMfdLbM9LLDRGSwNesr3Svm1nGCNWBEdg1ASAhvxSIBIAceoBLhZ96lwJQj5e62Qe17cB+sR19igkAmPSyEXcMXfrbSwAYwTE9/3Bcpy98jvZPeEwwXFg9xrAk8n6IWse6TQCYqedrZQCOOLZGPeUCqErh8QxLeJ4i4Xm0tecKgLUxcOq6Ml+NxnGbBYCNKMhV4wG4bNcIeUL93O47eO+E7EBEXJXCwyx6okQD9MMBE4CUHJ85gPtJNGOLFJL7gHoqN5wKAG8aU5O+RTwf41a8rhPNcFMBcABaB+uJg+qVOjnFCdL07yUGri6MGpkteNcOIKht66b3eFZ27kaKsYwDwwoAb+gtWkpOBjD8LzZek+IAUBpaBYDwUkUkqGRqukyxSN+zIAUpDqBjxIea0pfkz82UArCDiUDiMRjP+E8N/WicmQKpjaDT45nnM8kwEUCsIn6dq/ubDOAIhy93lQdASGy0h9UBsEKTzdxl9dfJABaqQde1a3QFb9e2lwIwgSY2UksdXd9+DvzYeBXc6buRImOM1Q2LqE4gbpUHxKYcSibIsHwi5wMQ+5zmSQAGhX7dx4TAf/uaZQD/DIKJTEbpatwgJQG04nLOTlDbeep1QcqXuQUHjDnfxuQzlmN6fMBbbBUC6F0IONRagKWuP26eD8Cy5DrGBPg3jWIZCgAOSOD9D+JwrCQA5xZ2aphNQ+oR+ZU6mQEjqa3+Zl4CzCu+LARwzNVlAGbQM2ht5wMQdl9gZ0exvplu06WRuy8B4J3jLgwHKPasJYBgAwj4pI8weLLEolETFIQ7NOVeM1azY8GUkHuYE/xZCCD87q4JQFMKk43txeIAKv+isjYoOcEDN6YVLRPqClAkuSgBiDgGIYyNGmc2JiwBTK+AwHeci1BbDLVQ+9Xr4CxQGuMzJCSDz9pAN3bvrWQSCMC34cZ5ujtNABIsTz9uLQqgxgui5AErU8zXwKpyZqwy8u8KAJzyIfXgXlIqEeJWdZkedOsPSoCjpSuhF1b9awQfXFVXAL0tD4PPx9g8naZy+CNe7wuUmQhddSs1xIu3VgB2uwQAfQLoHDFElgZAIE/v87PE0gszSoIzBUhf2BLd1HO5rnG/nJ6JPGCFqbtjAuBqYddXl5kJzsfkHCptNX/N90YBEI3xYvkVL3YUAJEQCviWlHQvoybCDEhhNcoGLi09TE2z0q7vgakWSDN175jK4T12/UmZALowBwuPGpsPW5YAaOmDAA51E/6bjQ9MWAPg0iaXV7TWo/8mfD5csA1xx9L2ZupMAHpwiy3c7lEBuCJs0eyXWQxt4j7cth6Po5j9ttz+nUoWr81L5bBDZGlavo7HzdMAVA+IAp/6ms4vPKbf5wlI0mI7YIWYq0FWNQWzph2hNLOa97+UB8C5JBwwc0VHj9EXo+bJCQXAKn9B5EwAyGfTgCfP91H4ltu1D0Hz/JmzlwEQVvflB1UAtewhvYHyAJC7iD6851yWExgjPHeIHFMC8AfbvUqL2uFUAG3cm2CiwzcGZoVSnvSAGu5nHxELAPt9QidlV3iemUC/qzwAVcz/NOTMlSE39g6PDMBxgKwTotg7FQA3mQHceg1ppuqPSveU2BfsbdnzEisAde1sv7ZbBUDYZpGx9SBJtgIkJ42lYCiO2/FL2B3FU1/yoqoeRzK3iWudtmgZtoPnGad8gvaSgd2Ld+KC5ekK6zMXHY3jwAXVXEdSs5NJzT8XGQpdkbbp4dwheLMTR3htDRnBFrzcTfaUoZv/47+MOW7d6bwXdVURW2yxxRZbbLHFFltsscUWW2yxxRZbbLHFFltsscUWW2w5o/wHLOOrA1HX/EEAAAAASUVORK5CYII=" alt="Slim"/></a>
            </header>
            <h1>Welcome to Slim!</h1>
            <p>
                Congratulations! Your Slim application is running.
            </p>
            <section>
                <h2>Get Started</h2>
                <ol>
                    <li>The application code is in <code>index.php</code></li>
                    <li>Read the <a href="http://docs.slimframework.com/" target="_blank">online documentation</a></li>
                    <li>Follow <a href="http://www.twitter.com/slimphp" target="_blank">@slimphp</a> on Twitter</li>
                </ol>
            </section>
            <section>
                <h2>Slim Framework Community</h2>

                <h3>Support Forum and Knowledge Base</h3>
                <p>
                    Visit the <a href="http://help.slimframework.com" target="_blank">Slim support forum and knowledge base</a>
                    to read announcements, chat with fellow Slim users, ask questions, help others, or show off your cool
                    Slim Framework apps.
                </p>

                <h3>Twitter</h3>
                <p>
                    Follow <a href="http://www.twitter.com/slimphp" target="_blank">@slimphp</a> on Twitter to receive the very latest news
                    and updates about the framework.
                </p>
            </section>
            <section style="padding-bottom: 20px">
                <h2>Slim Framework Extras</h2>
                <p>
                    Custom View classes for Smarty, Twig, Mustache, and other template
                    frameworks are available online in a separate repository.
                </p>
                <p><a href="https://github.com/codeguy/Slim-Extras" target="_blank">Browse the Extras Repository</a></p>
            </section>
        </body>
    </html>
EOT;
        echo $template;
    }
);
	/*
	* METODOS CRUD
	* https://websitebeaver.com/php-pdo-prepared-statements-to-prevent-sql-injection
	*/

	$app->get('/hello/:name', function ($name) {
	    echo "Hello, " . $name;
	});

	$app->get('/Usuario','verifyApiKey','UsuarioController:SelUsuarioAll');
	$app->get('/Usuario/:codigo','verifyApiKey','UsuarioController:SelUsuario');
	$app->post('/Usuario','verifyApiKey','UsuarioController:InsUsuario');
	$app->put('/Usuario','verifyApiKey','UsuarioController:UpdUsuario');
	$app->delete('/Usuario/:codigo','verifyApiKey','UsuarioController:DelUsuario');

	$app->get('/Almacen','verifyApiKey','AlmacenController:SelAlmacenAll');

    $app->notFound(function () use ($app) {
        $response = array('status'=>false,'message'=>"Page Not Found");
        echo json_encode($response);
    });

	$app->run();

?>