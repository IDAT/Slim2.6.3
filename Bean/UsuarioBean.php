<?php
class UsuarioBean extends UsuarioModel implements JsonSerializable{
	
    protected $usu_estdes;
    
    public function __construct(array $data) {
    	parent::__construct($data);
        $this->usu_estdes= $data['usu_estdes'];
    }
    
    public function getUsuEstDes(){
        return $this->usu_estdes;
    }

    public function setUsuEstDes($estdes){
        $this->usu_estdes = $estdes;
    }

    public function jsonSerialize(){
        return 
        [
            'usu_codigo'   => $this->getUsuCodigo(),
            'usu_estdes'   => $this->getUsuEstDes()
        ];
    }
    //$Usuario = new UsuarioBean(array('usu_codigo' => '999', 'usu_nombre' => 'Alex'));
}
?>