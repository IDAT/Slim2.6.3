<?php
	abstract class UsuarioModel{
		protected $usu_codigo;
	    protected $usu_nombre;
	    protected $usu_passwd;
	    protected $usu_descri;
	    protected $usu_email;
	    protected $usu_fecreg;
	    protected $usu_imagen;
	    protected $usu_estcod;

		public function __construct(){

		}

	    public function __construct(array $data){
	        $this->usu_codigo = $data['usu_codigo'];
	        $this->usu_nombre = $data['usu_nombre'];
	        $this->usu_passwd = $data['usu_passwd'];
	        $this->usu_descri = $data['usu_descri'];
	        $this->usu_email  = $data['usu_email'];
	        $this->usu_fecreg = $data['usu_fecreg'];
	        $this->usu_imagen = $data['usu_imagen'];
	        $this->usu_estcod = $data['usu_estcod'];
	    }

	    public function getUsuCodigo(){
	        return $this->usu_codigo;
	    }

	    public function setUsuCodigo($codigo) {
        	$this->usu_codigo = $codigo;
    	}

	    public function getUsuNombre(){
	        return $this->usu_nombre;
	    }

	    public function setUsuNombre($nombre) {
        	$this->usu_nombre = $nombre;
    	}

	    public function getUsuPasswd(){
	        return $this->usu_passwd;
	    }

	    public function setUsuPasswd($passwd) {
        	$this->usu_passwd = $passwd;
    	}

	    public function getUsuDescri(){
	        return $this->usu_descri;
	    }
	    public function getUsuEmail(){
	        return $this->usu_email;
	    }
	    public function getUsuFecReg(){
	        return $this->usu_fecreg;
	    }
	    public function getUsuImagen(){
	        return $this->usu_imagen;
	    }
	    public function getUsuEstCod(){
	        return $this->usu_estcod;
	    }

	}
?>